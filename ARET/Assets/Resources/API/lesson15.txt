+
lessonId*15
+
w*1*clergy*n*churchman, ministry*روحانیت، روحانیون*Catholic clergy are not allowed to marry.*روحانیون کاتولیک اجازه ندارند ازدواج کنند.*ImageAdress*AR_Adress
w*2*popular*adj*accepted, prevailing, approved *پرطرفدار، محبوب، مردم پسند*The Beatles wrote many popular songs.*گروه بیتلها ترانه های پر طرفدار زیادی را تصنیف کردند.*ImageAdress*AR_Adress
w*3*rival*n*adversary, competitor, challenger, opponent*رقیب، حریف*She and I are rivals for the swimming prize.*من و او برای بردن جایزه شنا رقیب هم هستیم.*ImageAdress*AR_Adress
w*4*shriek*v,n*scream, screech*جيغ زدن، جیغ کشیدن *a.	The maid shrieked when she discovered the corpse*وقتی خدمتکار جنازه را پیدا کرد، جیغ کشید*ImageAdress*AR_Adress
w*5*stationary*adj*fixed, immobile, motionless *ثابت، غیر متحرک، ساکن. بی حرکت، متوقف*A factory engine is stationary.*موتور کارخانه ثابت است*ImageAdress*AR_Adress
w*6*uneasy*adj*impatient , nervous , unquiet*ناآرام، پریشان؛ معذب، ناراحت؛ مضطرب، بی قرار نگران، دلواپس*I had an uneasy feeling that someone was watching me.*ناراحت بودم زیرا احساس می کردم کسی مرا می پاید.*ImageAdress*AR_Adress
w*7*vicinity*n*neighborhood, environs, proximity*حول و حوش، اطراف، نزدیکی، مجاورت*There are two parks in the vicinity of their house*در نزدیکی خانه آنها دو پارک است.*ImageAdress*AR_Adress
w*8*subconscious*n*feelings, desires etc are hidden in your mind and affect your behaviour, but you do not know that you have them*نیمه هوشیار، نیم آگاه*We are not able to understand the subconscious desire of others.*ما نمی توانیم تمایلات ناخودآگاه (نیمه هوشیار) دیگران را بفهمیم.*ImageAdress*AR_Adress
w*9*successive*adj*consecutive, in a row, sequential, in succession*پی در پی، متوالی؛ پیاپی، بعدی، متعاقب*This was their fifth successive win.*این پنجمین پیروزی پی در پی آنها بود*ImageAdress*AR_Adress
w*10*former*adj*previous, preceding, prior, recent, first *پیشین، قبلی، سابق*Bob had to choose between giving up his job and giving up his principles. He chose the former.*باب مجبور بود بین ترک کردن شغلش و دست کشیدن از اصول یکی را انتخاب کند. او اولی را انتخاب کرد.*ImageAdress*AR_Adress
w*11*latter*n*later, end, final, second *دوم، دومی؛ دیگری؛ پایانی؛ *The latter point is more important than the former. *نکته ی دومی از اولی مهم تر است.*ImageAdress*AR_Adress
w*12*conflagration*n*great fire, blaze *حریق بزرگ، آتش سوزی عظیم*A cow caused the conflagration in Chicago.*یک گاو باعث حریق بزرگ در شیکاگو شد.*ImageAdress*AR_Adress
w*13*degrade*v*make contemptible, lower, abase *تحقیر کردن، کوچک کردن، خوار کردن؛ خراب کردن، ضایع کردن*This poster is offensive and degrades women.*این پوستر اهانت آمیز است و زنان را تحقیر میکند.*ImageAdress*AR_Adress
w*14*indigent*adj*poor, needy, beggared *فقیر، تنگدست، تهیدست*Because he was indigent, he was sent to the welfare office.*چون فقیر بود به ادارهی بهزیستی فرستاده شد.*ImageAdress*AR_Adress
w*15*lax*adj*careless, negligent, heedless, easy-going*اهمال کار، آسان گیره بی توجه، بی دقت، شل، سست*The company has been lax in carrying out its duties.*شرکت در انجام وظایفش اهمال کار بوده است.*ImageAdress*AR_Adress

+
t*1*Test Yourself*From the following list, use each word only once to complete the sentences below. Remember that in the case of nouns and verbs you may need to change the form of the word: rival (n), degrade (v), conflagration (n), successive (adj), vicinity (n), lax (adj), former (n), shriek (v)
+
q*1*The two sisters have always been....
q*2*The children were.....with laughter.
q*3*The stolen car was found in the..... of the station.
q*4*This was their fifth.....win.
q*5*The company has been .... in carrying out
q*6*The dolphin's habitat is being rapidly....
q*7*Of the two possibilities the ... seems more likely.
q*8*In the .... that followed the 1960's earthquake, much of San Francisco was destroyed.
+
a*1*rivals
a*2*shrieking
a*3*vicinity
a*4*successive
a*5*lax
a*6*degraded
a*7*former
a*8*conflagration



