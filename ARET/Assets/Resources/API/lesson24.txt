+
lessonId*24
+
w*1*benign*adj*benevolent; amiable, gentle, kind; harmless*بی خطر، خوش خیم؛ رئوف، مهربان، ملایم، بی آزار*The tumour is not malignant, It is benign.*تومور (غده) بدخیم نیست. آن خوش خیم است*ImageAdress*AR_Adress
w*2*chronological*adj*arranged in order in which the events happened*به ترتیب زمان، به ترتیب تاریخ، زمانی*This book describes the main events in his life in chronological order.*این کتاب رویدادهای اصلی زندگی او را به ترتیب زمانی توصیف میکند.*ImageAdress*AR_Adress
w*3*connive*v*conspire, intrigue, collude *همدستی کردن، مشارکت کردن، تبانی کردن؛ نادیده گرفتن، تجاهل کردن*They connived with their mother to deceive me.*آنها با مادرشان تبانی کردند تا مرا فریب دهند.*ImageAdress*AR_Adress
w*4*decadent*adj*corrupt, debased, decaying, Immoral *رو به انحطاط، رو به زوال، منحط؛ اسم آدم منحط، فاسد*Tom talked about a decadent society.*تام در مورد یک جامعه ی رو به زوال صحبت کرد.*ImageAdress*AR_Adress
w*5*devious*adj*cunning, deceitful, sly; indirect*خلاف نادرست؛ إشخصیت تودار مرموز*He got rich by devious means.*او با نادرستی ثروتمند شده*ImageAdress*AR_Adress
w*6*digress*n*deviate; get off the subject, lose the thread *دور افتادن، منحرف شدن از؛ از شاخه ای به شاخه ای پریدن*Keep to the subject, don't digress!*به اصل مطلب بپرداز، حاشیه نرو*ImageAdress*AR_Adress
w*7*belligerent*adj*warlike, aggressive, quarrelsome, violent*پرخاشگر، جنگ طلب، ستیزه جو؛ ستیزه جویانه، پرخاشگرانه، خصمانه؛ خشن؛ در حال جنگ*Mary always seems belligerent towards me.*مری همیشه نسبت به من پرخاشگر به نظر می رسد.*ImageAdress*AR_Adress
w*8*benevolent*adj*kindly, charitable, humane, humanitarian *نیکخواه خیر، خیراندیش؛ نیکخواهانه، خیراندیشانه، از روی مهربانی*A benevolent man helped them.*مرد خیری به آنها کمک کرد*ImageAdress*AR_Adress
w*9*bizzare*adj*odd, peculiar, strange, weird*عجیب و غریب، غیرعادی؛ باورنکردنی*The plot of the novel was too bizarre to be believed.*طرح داستان آنقدر عجیب و غریب بود که نمی شد آن را باور کرد.*ImageAdress*AR_Adress
w*10*cajole*v*coax, flatter, lure *با چرب زبانی وادار به (کاری) کردن، با زبان بازی (کسی را) راضی*He really knows how to cajole people into doing what he wants.*او واقعا می داند چطور مردم را با چرب زبانی به انجام کاری که می خواهد وادار کند.*ImageAdress*AR_Adress
w*11*candor*n*frankness , honesty , truthfulness*صداقت، صراحت، رک گویی، صمیمیت، بی ریایی*Even though I didn't agree with him, I liked his candor*با آن که با او موافق نبودم از صداقت او خوشم آمد.*ImageAdress*AR_Adress
w*12*manifold*adj*complex, many, numerous, various *متعدد، بسیار زیاد؛ متنوع، گوناگون؛ چند جانبه، چند کاره*The possibilities were manifold*امکانات بسیار زیاد بوده*ImageAdress*AR_Adress
w*13*nebulous*adj*unclear, vague, abstract *افکار، خاطرات و غیره مغشوش، در هم بر هم، مبهم، آشفته، گنگ، نامشخص*Your theories are too nebulous please clarify*نظرات شما خیلی مهم هستند؛ لطفا آنها را روشن کنید.*ImageAdress*AR_Adress
w*14*pensive*adj*thoughtful, reflective, meditative*حالت، چهره فکورانه، متفکرانه؛ غمگین*Helen looked pensive.*هلن به نظر غرق در فكر (غمگین) بود*ImageAdress*AR_Adress
w*15*propagate*v*produce, multiply, spread, distribute*اشاعه دادن، ترویج دادن، پخش کردن؛ *He planned to propagate his knowledge.*او قصد داشت دانش خود را اشاعه دهد.*ImageAdress*AR_Adress
+
t*1*Test Yourself*Choose the synonym. 
+
q*1*connive
q*2*devious
q*3*bizarre
q*4*cajole
q*5*pensive
+
a*1*conspire*assist*advocate*persuade. 
a*2*cunning*plain*moral*honest
a*3*odd*familiar*ordinary*current
a*4*coax*mention*criticises*remind
a*5*thoughtful*irresponsible*forgetful*careless


