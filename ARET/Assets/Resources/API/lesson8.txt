+
lessonId*8
+
w*1*budget*n*allowance, cost, finances, funds*بودجه، حساب دخل و خرج، هزينه*We have a weekly budget for food.*ما برای غذا بودجه ی هفتگی داریم.*ImageAdress*AR_Adress
w*2*burden*n*load, cargo, charge *بار سنگین، بار مسئولیت، فشار، سنگینی*The burden of the country's safety is in the hands of the President.*بار مسئولیت امنیت کشور بر عهدهی رئیس جمهور است*ImageAdress*AR_Adress
w*3*dispute*n,v*(n) argument, conflict; (v) disagree, oppose*اسم مشاجره، جروبحث: (فعل) جروبحث کردن، مشاجره کردن؛ مبارزه کردن مقاومت کردن*here is a pay dispute at the factory and the workers are on strike.*در کارخانه مشاجره ی مالی وجود دارد و کارگران در اعتصاب هستند.*ImageAdress*AR_Adress
w*4*loyalty*n*faithfulness, fidelity *وفاداری* The government was sure of the people's loyalty. *دولت از وفاداری مردم مطمئن بود.*ImageAdress*AR_Adress
w*5*precaution*n*caution, providence, prudence; foresight*احتیاط، احتیاط کاری، اقدام احتیاطی، دوراندیشی؛ پیش بینی*I took the precaution of locking all the windows when I went out.*وقتی بیرون رفتم اقدام احتیاطی را به عمل آوردم و همه ی پنجره ها را قفل کردم.*ImageAdress*AR_Adress
w*6*reptile*n*kinds of animal*مارمولک*The lizard is a reptile with a very slender body.*مارمولک، خزنده ای است که دارای بدن بسیار باریک است.*ImageAdress*AR_Adress
w*7*shrill*adj*acute, high-pitched*تیز، گوشخراش، تند، شدیداللحن*I heard a shrill voice.*صدای گوشخراشی شنیدم.*ImageAdress*AR_Adress
w*8*render*v*cede, deliver; present; perform; cause to be, make*انجام دادن، ارائه کردن؛ کمک رساندن*he rendered her role skillfully.*او نقش خود را با مهارت اجرا کرد.*ImageAdress*AR_Adress
w*9*paperback*n*a book with a stiff paper cover .*کتاب جلد نرم / نازک، کتاب جلد شومیز*His new dictionary came out in paperback*فرهنگ لغت جدید او با جلد شومیز منتشر شد.*ImageAdress*AR_Adress
w*10*represent*v*denote, symbolize, show, stand for *نشان دادن، نمایاندن؛ نماینده (کسی / جایی) بودن، نمایانگر (چیزی) بودن؛ دلالت داشتن بره نماد / مظهر (چیزی) بودن*Phonetic symbols represent sounds.*علائم آواشناسی نمایانگر صداها (واکه ها) هستند.*ImageAdress*AR_Adress
w*11*alternatively*adv*used to suggest something that someone could do or use instead of something else *راه دیگر، راه دیگر این که*We could take the train or alternatively go by car.*ما می توانستیم قطار سوار شویم یا به جای آن با اتومبیل برویم.*ImageAdress*AR_Adress
w*12*appalled*adj*dismayed, shocked, hideous*منزجر، بیزار، متنفر؛ شوکه، وحشت زده*When I heard what had happened I was absolutely appalled.*وقتی شنیدم چه اتفاقی افتاده، کاملا شوکه شدم.*ImageAdress*AR_Adress
w*13*calumny*n*false accusation, slander*افترا، تهمت، بهتان*can't bear the calumny.*افترا را نمی توانم تحمل کنم.*ImageAdress*AR_Adress
w*14*drudgery*n*hard work, donkey - work, toll*کار شاق / سخت، کار پرزحمت، خرحمالی*She thought about the drudgery of work in rug weaving workshops.*او در مورد کار شاق در کارگاههای فرشبافی فکر می کرد.*ImageAdress*AR_Adress
w*15*encomium*n*high praise, applause, laudation *ثنا، مدح، ستایش، تمجید؛ (شعر) مديحه*His book is worthy of encomium.*کتاب او در خور ستایش است.*ImageAdress*AR_Adress

+
t*1*Test Yourself*Choose the best ending for each of the extracts below from the list underneath.
+
q*1*I expect loyalty ... 
q*2*They rendered ... 
q*3*Vets took precautions ... 
q*4*He was appalled ... 
q*5*He was sickened by the encomium ... 
q*6*I don't like being a burden ...

+
a*1*... from my family
a*2*. ... assistance to the disaster victims. 
a*3*... to prevent the spread of the disease.
a*4*... at how dirty the place was. 
a*5*... expressed by the speakers. 
a*6* ... on other people

