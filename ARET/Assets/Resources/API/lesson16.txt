+
lessonId*16
+
w*1*confident*adj*certain, assured, self-confident, sure*مطمئن، خاطر جمع؛ اطمینان بخش، حاکی از اعتماد به نفس*I was confident that I had passed the examination.*من مطمئن بودم که در امتحان قبول شده ام.*ImageAdress*AR_Adress
w*2*corpse*n*a dead body, cadaver*جسد، جنازه، نعش*When given all the data on the corpse, Columbo was able to solve the murder.*زمانی که تمام اطلاعات درباره ی جسد به "کلمبو" داده شد، او توانست پرده از روی قتل بردارد.*ImageAdress*AR_Adress
w*3*mute*adj*silent, noiseless, dumb*ساکت، آرام، بی صدا: لال، گنگ*The usually defiant child stood mute before the principal.*بچه ای که اغلب گستاخ بود، در حضور مدیر ساکت ایستاد*ImageAdress*AR_Adress
w*4*promote*v*advance ; help ; advertise*ترفيع دادن به، ارتقا دادن به مقامی ارتقا یافتن؛ کمک کردن؛ (کالا) تبلیغ کردن، ترویج دادن*Our teacher has been promoted to headmaster.*معلم ما به مقام مدیر مدرسه ارتقا یافته است.*ImageAdress*AR_Adress
w*5*soothe*v*quiet ; calm ; comfort*آرام کردن، تسکین دادن*She soothed the child who was afraid.*او کودک را که ترسیده بود آرام کرد.*ImageAdress*AR_Adress
w*6*traitor*n*betrayer, double-crosser*خائن، خیانتکار؛ وطن فروش*The traitor was sent to prison*خائن به زندان افتاد.*ImageAdress*AR_Adress
w*7*vermin*n*parasites , pasts*حشرات موذی / انگلی، جانوران موذی آفات جانوری*On farms the fox is considered vermin.*در مزارع روباه جانور موذی به حساب می آید.*ImageAdress*AR_Adress
w*8*conceptual*adj*dealing with ideas, or based on them *مفهومی، ذهنی، عقلی؛ ادراکی، بینشی*He established a conceptual framework within which to consider the issues.*او یک چارچوب مفهومی تشکیل داد تا در آن موضوعات را بررسی کند.*ImageAdress*AR_Adress
w*9*convinced*adj*sure, positive, reassured*مطمئن، متقاعد؛ مومن، معتقد*Peter is convinced of Mary's success.*پیتر در مورد موفقیت مری مطمئن است.*ImageAdress*AR_Adress
w*10*critical*adj*criticizing; serious, crucial, vital *موشکافانه، نقادانه؛ وضعیت بیمار*Students should develop critical thinking instead of accepting everything they are told without question it.*دانشجویان به جای این که هر چیزی را که به آنها گفته میشود بدون چون و چرا قبول کنند باید نقادانه بیندیشند.*ImageAdress*AR_Adress
w*11*devise*v*invent, create; design*اختراع کردن، ابداع کردن*She devised a new computer program.*او برنامه کامپیوتری جدیدی را ابداع کرد.*ImageAdress*AR_Adress
w*12*concomitant*n*be with something or someone*ملازم، همراه، توأم، مقارن، هم زمان*Deafness is a frequent concomitant of old age.*ناتوانی غالبا با پیری همراه است.*ImageAdress*AR_Adress
w*13*deleterious*adj*bad , harmful , disadvantageous*مضر، زیان بار، زیان بخش*You should avoid the deleterious effects of radioactive substances.*باید از تأثیرات مضر مواد رادیواکتیو اجتناب کنید.*ImageAdress*AR_Adress
w*14*metamorphosis*n*change, alternation, transformation*دگردیسی، دگرگونی، تغییر بنیادی؛ استحاله؛ مسخ*The metamorphosis of a butterfly is interesting*دگردیسی یک پروانه جالب است.*ImageAdress*AR_Adress
w*15*pernicious*adj*harmful, hurtful, dangerous*مخرب، مضر، زیان بار؛ خطرناک، وخیم*This magazine has a pernicious effect on young people.*این مجله تأثیر زیان باری روی افراد جوان دارد.*ImageAdress*AR_Adress

*
t*1*Test Yourself*Choose the correct meaning of each underlined word.
+
q*1*The doctor gave me some skin cream to soothe the irritation. 
q*2*She denied that she had turned traitor.
q*3*They've devised a plan for keeping traffic out of the city centre. 
q*4*workers in nuclear research must avoid the deleterious effects of radioactive substances.
q*He argued that these books had a pernicious effect on young and susceptible minds.

+
a*1*quiet*annoy*abuse*burn
a*2*betrayer*defender*loyalist*supporter
a*3*invented*established*designed*formed
a*4*harmful*gentle*tranquil*beneficial
a*5*dangerous*benign*non-toxic*acceptable
