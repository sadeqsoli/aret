﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class SignupManager : MonoBehaviour
{
    #region Properties


    #endregion

    #region Fields

    [Header("General")]
    [SerializeField] GameObject[] S_PL_GL_Canvas;

    [Space]
    [SerializeField] TextMeshProUGUI title_GL_TXT, explain_GL_TXT;
    [SerializeField] Image logoIMG_GL, logoIMG_PL_Sadeq, logoIMG_PL_Shiva;
    const float delay = 0.02f;


    [Header("Login")]
    [SerializeField] TMP_InputField nameField;
    [SerializeField] Button goToMainMenu;


    #endregion

    #region Public Methods

    public void RestValues()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.Save();
        Debug.Log("Deleted!");
    }


    void UsernameValidation(string username)
    {
        //char[] a = username.ToCharArray();
        //a[0] = char.ToUpper(a[0]);
        if (username.Length < 3)
        {
            goToMainMenu.interactable = false;
        }
        else if (username.Length >= 3)
        {
            goToMainMenu.interactable = true;
        }
    }
    #endregion




    #region Private Methods
    void Awake()
    {
        //RestValues();
        StartCoroutine(SplashScreenPlan());
        goToMainMenu.interactable = false;
        nameField.onValueChanged.AddListener(UsernameValidation);
    }//Awakeeeee

    IEnumerator SplashScreenPlan()
    {
        SetOBJ_Off_On(0);
        yield return StartCoroutine(Fade_IN_OUT_Splash(title_GL_TXT, logoIMG_GL, explain_GL_TXT, null));


        SetOBJ_Off_On(1);
        yield return StartCoroutine(Fade_IN_OUT_Splash(null, logoIMG_PL_Sadeq, null, logoIMG_PL_Shiva));

        SignUpCheck();

    }



    #region SignUp Process






    #endregion//SignUp Process

    #region Intro Panel
    void GoToMain()
    {
        StartCoroutine(Login());
    }
    IEnumerator Login()
    {
        UserRepo.PushUsername(nameField.text);
        yield return new WaitForSeconds(delay);
        H_SceneManager.GoToNextOrPrevScene(true);
    }
    #endregion


    #region Shared...


    void SetOBJ_Off_On(int numb)
    {
        for (int i = 0; i < S_PL_GL_Canvas.Length; i++)
        {
            if (i == numb)
            {
                S_PL_GL_Canvas[i].SetActive(true);
            }
            else
            {
                S_PL_GL_Canvas[i].SetActive(false);
            }

        }
    }


    void SignUpCheck()
    {
        if (!(PlayerPrefs.HasKey(UserRepo.RepoUser)))
        {
            SetOBJ_Off_On(2);
            goToMainMenu.onClick.AddListener(GoToMain);
        }
        else
        {
            H_SceneManager.GoToNextOrPrevScene(true);
        }
    }
    #endregion//Shared...

    #region Splash Screen Code
    IEnumerator Fade_IN_OUT_Splash(TextMeshProUGUI logoText1, Image logo1, TextMeshProUGUI logoText2 = null, Image logo2 = null)
    {
        if (logoText1 != null)
            logoText1.gameObject.SetActive(true);
        if (logoText2 != null)
            logoText2.gameObject.SetActive(true);
        if (logo1 != null)
            logo1.gameObject.SetActive(true);
        if (logo2 != null)
            logo2.gameObject.SetActive(true);

        // fade from transparent to opaque
        // loop over 1 second
        for (float j = 0; j <= 1; j += Time.deltaTime)
        {
            if (j > 0.1)
            {

            }
            // set color with i as alpha
            if (logoText1 != null)
                logoText1.color = new Color(logoText1.color.r, logoText1.color.g, logoText1.color.b, j);
            if (logoText2 != null)
                logoText2.color = new Color(logoText2.color.r, logoText2.color.g, logoText2.color.b, j);
            if (logo1 != null)
                logo1.color = new Color(logo1.color.r, logo1.color.g, logo1.color.b, j);
            if (logo2 != null)
                logo2.color = new Color(logo2.color.r, logo2.color.g, logo2.color.b, j);
            yield return new WaitForSeconds(delay);
        }

        yield return new WaitForSeconds(1f);

        // fade from transparent to opaque
        // loop over 1 second
        for (float j = 1; j >= 0; j -= Time.deltaTime)
        {
            // set color with i as alpha
            if (logoText1 != null)
                logoText1.color = new Color(logoText1.color.r, logoText1.color.g, logoText1.color.b, j);
            if (logoText2 != null)
                logoText2.color = new Color(logoText2.color.r, logoText2.color.g, logoText2.color.b, j);
            if (logo1 != null)
                logo1.color = new Color(logo1.color.r, logo1.color.g, logo1.color.b, j);
            if (logo2 != null)
                logo2.color = new Color(logo2.color.r, logo2.color.g, logo2.color.b, j);
            yield return new WaitForSeconds(delay);
        }
        if (logoText1 != null)
            logoText1.gameObject.SetActive(false);
        if (logoText2 != null)
            logoText2.gameObject.SetActive(false);
        if (logo1 != null)
            logo1.gameObject.SetActive(false);
        if (logo2 != null)
            logo2.gameObject.SetActive(false);
    }

    #endregion//Splash Screen Code



    void Update()
    {

    }//Updateeeee

    #endregion
}//EndClasssss
