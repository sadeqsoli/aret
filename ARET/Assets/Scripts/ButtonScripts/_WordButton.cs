﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public class _WordButton : MonoBehaviour
{
    public bool IsWordPronounced { get { return isWordPronounced; } }

    bool isWordPronounced;

    //Image Explainationary to current Word.
    [SerializeField] RawImage Ex_Image;

    //this is for changing the Color of Button.
    [SerializeField] Image ButtonIMG;

    //This is to change color of IMG beneeth word number. 
    [SerializeField] Image WordNumbIMG;

    //Image Explainationary to current Word.
    [SerializeField] TextMeshProUGUI WordTXT;

    //word Number Text.
    [SerializeField] TextMeshProUGUI WordNumbTXT;

    //Opening WordStructure.
    [SerializeField] Button WordButton;

    //Pronounciation Buttons.
    [SerializeField] Button UK_Button, US_Button;


    public void GetEx_Image(Texture2D newTexture)
    {
        Ex_Image.texture = newTexture;
    }
    public void GetButtonColor(Color newColor)
    {
        ButtonIMG.color = newColor;
    }
    public void GetWordNumbColor(Color newColor)
    {
        WordNumbIMG.color = newColor;
    }
    public void GetWordTXT(string text)
    {
        WordTXT.text = text;
    }
    public void GetWordNumbTXT(string text)
    {
        WordNumbTXT.text = text;
    }

    public void ChangeWordButtonListener(UnityAction unityAction)
    {
        WordButton.onClick.RemoveAllListeners();
        WordButton.onClick.AddListener(unityAction) ;
    }
    public void Change_UK_VoiceButtonListener(UnityAction unityAction)
    {
        UK_Button.onClick.RemoveAllListeners();
        UK_Button.onClick.AddListener(unityAction);
    }
    public void Change_US_VoiceButtonListener(UnityAction unityAction)
    {
        US_Button.onClick.RemoveAllListeners();
        US_Button.onClick.AddListener(unityAction);
    }


}//EndClasssss
