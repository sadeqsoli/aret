﻿using System.Collections.Generic;


[System.Serializable]
public class _____Lesson
{
    public int LessonId { get; set; }

    public List<Word> Words { get; set; }

    public Exam Exam { get; set; }
}
public class Word
{
    public int WordID { get; set; }
    public string AR_Object { get; set; }
    public string WordName { get; set; }
    public string MeaningFA { get; set; }
    public string Synonym { get; set; }
    public string Example_EN { get; set; }
    public string Example_FA { get; set; }
}
public class Exam
{
    public int ExamID { get; set; }

    public List<Question> Questions { get; set; }
    public List<Answers> Answers { get; set; }

}
public class Question
{
    public int QuestionID { get; set; }
    public string QuestionType { get; set; }
    public string TheQuestion { get; set; }
}
public class Answers
{
    public int AnswerID { get; set; }
    public string[] AnswersText { get; set; }
}

