﻿using System.Collections;
using RTLTMPro;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class _Word : MonoBehaviour
{
    //Image Explainationary to current Word.
    public Image[] Ex_Image;

    //current Word Text.
    public TextMeshProUGUI WordTXT;

    //To showing  AR Object.
    public Button AR_Button, PronounExam;

    //Pronounciation Buttons.
    public Button UK_Button_W, US_Button_W;

    //Meaning English.
    public TextMeshProUGUI Meaning_EN;
    //Meaning Persain
    public RTLTextMeshPro Meaning_Fa;

    //Example English.
    public TextMeshProUGUI Example_EN;
    //Example Persain
    public RTLTextMeshPro Example_Fa;
    //Pronounciation Buttons.
    public Button UK_Button_E, US_Button_E;

}
