﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class _LessonButton : MonoBehaviour
{
    public int TaxNumber { get; set; }

    [SerializeField] Button _CurrentLesson_Btn, _Download_Btn;

    [SerializeField] Image _LessonIMG,_StatusIMG;

    [SerializeField] Image _DownFillIMG;

    [SerializeField] GameObject _DownPNL, _CompleteDownload;

    [SerializeField] TextMeshProUGUI _LessonName,_CoinNumberTXT;

    float progress = 0f;

    //showing download progress by number and visual.
    float delay = 0.01f;

    public void SetLessonBtn_Listener(UnityAction unityAction)
    {
        _CurrentLesson_Btn.onClick.RemoveAllListeners();
        _CurrentLesson_Btn.onClick.AddListener(unityAction);
    }
    public void GetLessonColor(Color newColor)
    {
        _LessonIMG.color = newColor;
    }
    public void GetLesson_IMG(Sprite newSprite)
    {
        _LessonIMG.sprite = newSprite;
    }
    public void GetStatus_IMG(Sprite newSprite)
    {
        _StatusIMG.sprite = newSprite;
    }
    public void GetLessonNameTXT(string lessonName)
    {
        _LessonName.text =  lessonName;
    }
    public void GetCoinTXT(int taxNumb)
    {
        _CoinNumberTXT.text =  taxNumb.ToString();
    }

    public void CompleteDownload(bool isDowned)
    {
        _CompleteDownload.SetActive(false);
        if (isDowned)
        {
            StartCoroutine(SetInactive());
        }
        
    }
    public void GetDownPanelStatus(bool isDowned)
    {
        if (isDowned)
        {
            _DownPNL.SetActive(true);
        }
        else
        {
            _DownPNL.SetActive(false);
        }
    }
    public void SetDownloadBtn_Listener(UnityAction unityAction)
    {
        _Download_Btn.onClick.RemoveAllListeners();
        _Download_Btn.onClick.AddListener(unityAction);
    }


    IEnumerator SetInactive()
    {
        _CompleteDownload.SetActive(true);
        yield return new WaitForSeconds(1f);
        _CompleteDownload.SetActive(false);
    }
}
