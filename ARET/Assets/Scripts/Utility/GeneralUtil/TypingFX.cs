﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class TypingFX : MonoBehaviour
{
    #region Public Variables
    public float delay = 0.1f;
    public string fullText;
    #endregion

    #region Private Variables
    private string currentText = "";
    #endregion

    #region Public Methods

    #endregion

    #region Private Methods

    void Start()
    {
        StartCoroutine(TypingEng());
    }//Starttttt


    IEnumerator TypingEng()
    {
        for (int i = 0; i < fullText.Length + 1; i++)
        {
            currentText = fullText.Substring(0, i);
            this.GetComponent<Text>().text = currentText;
            yield return new WaitForSeconds(delay);
        }
        yield return new WaitForSeconds(2);
        gameObject.SetActive(false);
    }



    void Update()
    {

    }//Updateeeee

    #endregion
}//EndClasssss
