﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Matrix_WS 
{
    [System.Serializable]
    public struct rowData_WS
    {
        public string[] row_WS;
    }
    public rowData_WS[] rows_WS = new rowData_WS[15]; //Grid of 7x7

}//EndClassss

