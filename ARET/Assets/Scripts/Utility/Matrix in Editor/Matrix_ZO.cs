﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Matrix_ZO
{
    [System.Serializable]
    public struct rowData_ZO
    {
        public int[] row_ZO;
    }
    public rowData_ZO[] rows_ZO = new rowData_ZO[17]; //Grid of 7x7

}//EndClassss