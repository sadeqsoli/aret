﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;

public class UIZoomImage : MonoBehaviour
{
    #region Properties
    //Static Instance of UIZOOMIMAGE Class.
    public static UIZoomImage Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new UIZoomImage();
            }
            return instance;
        }
    }

    #endregion

    #region Fields
    static UIZoomImage instance;
    //scrollrect in Parent. 
    ScrollRect scrollRect;

    //Hold a Vector3 that is Zoom_OUT at its max Scale. 
    Vector3 initialScale = new Vector3(minZoomScale, minZoomScale, minZoomScale);

    //Hold a Vector3 that is Zoom_IN at its max Scale. 
    Vector3 maxScale = new Vector3(maxZoomScale, maxZoomScale, maxZoomScale);

    //Hold a Vector3 that is The Current Scale. 
    Vector3 currentScale;

    //Hold a Vector3 that is The Current Scale. 
    Vector2 initialAnchoredPos;

    //this variables is for changing scale.
    const float minZoomScale = 1f;
    float currZoomScale = minZoomScale;
    const float maxZoomScale = 2f;


    //A constant value for delay in zoom process and do it smoothly.
    const float delay = 0.0000000001f;

    //A constant value for speed in zoom process.
    const float zoomSpeed = 0.03f;

    //A constant value for speed in Snapping process.
    const float snapSpeed = 7f;

    //for calculating x and y in Snapping process.
    float ySnapper, xSnapper;

    //for calculating x and y in Zooming process.
    float yZoomer, xZoomer;

    //An Array of float numbers that is for grading zoom modifier and smoothing the process.
    float[] zoomGrades = new float[12];

    //value of previous and current touch position.
    float touchesPrevPosDifference, touchesCurPosDifference;

    //position of first and second touch.
    Vector2 firstTouchPrevPos, secondTouchPrevPos;

    //position of first and second touch.
    bool onlyOnce = false, isMaxedOut = false, isMinedOut = false;
    #endregion
    #region Public Methods
    public void Snap_Out(GameObject obj)
    {
        StartCoroutine(Zome_OUT(obj));
        RectTransform rectTransform = GetRectTransform(obj);
        SnapTo_OUT(rectTransform);
    }
    public void Snap_In(GameObject obj)
    {
        StartCoroutine(Zoom_IN(obj));
        RectTransform rectTransform = GetRectTransform(obj);
        SnapTo_IN(rectTransform);
    }
    #endregion
    #region Private Methods
    void Awake()
    {
        instance = this;
        isMinedOut = false;
        isMaxedOut = false;
        onlyOnce = false;
        zoomGrades = SetZoomGrades();
        currentScale = initialScale;
        scrollRect = GetComponentInParent<ScrollRect>();
        initialAnchoredPos = GetRectTransform(gameObject).anchoredPosition;
    }


    RectTransform GetRectTransform(GameObject obj)
    {
        return obj.GetComponent<RectTransform>();
    }

    // Tap Zooming
    #region Tap Zoom IN _ OUT With Snapping IN _ OUT-----------------------------------------------
    void SnapTo_OUT(RectTransform target)
    {

        Canvas.ForceUpdateCanvases();
        ScrollRect scrollRect = this.GetComponentInParent<ScrollRect>();
        RectTransform rectTransform = GetRectTransform(gameObject);

        Vector2 anchoredPos = rectTransform.anchoredPosition =
            (Vector2)scrollRect.transform.InverseTransformPoint(rectTransform.position)
        - (Vector2)scrollRect.transform.InverseTransformPoint(target.position);
    }
    void SnapTo_IN(RectTransform target)
    {
        Canvas.ForceUpdateCanvases();
        ScrollRect scrollRect = this.GetComponentInParent<ScrollRect>();
        RectTransform mapTransform = GetRectTransform(gameObject);
        Vector2 anchoredPos = /*rectTransform.anchoredPosition =*/
            (Vector2)scrollRect.transform.InverseTransformPoint(mapTransform.position)
        - (Vector2)scrollRect.transform.InverseTransformPoint(target.position);

        StartCoroutine(Snap_X(anchoredPos, mapTransform));
        StartCoroutine(Snap_Y(anchoredPos, mapTransform));
    }
    IEnumerator Snap_Y(Vector2 anchoredPos, RectTransform rectTransform)
    {
        float currY = rectTransform.anchoredPosition.y;
        if (anchoredPos.y > 0)
        {
            for (float j = currY; j < anchoredPos.y; j += snapSpeed)
            {
                ySnapper = j;
                rectTransform.anchoredPosition = new Vector2(xSnapper, j);
                yield return new WaitForSeconds(delay);
            }
        }
        else if (anchoredPos.y < 0)
        {
            for (float j = currY; j > anchoredPos.y; j -= snapSpeed)
            {
                ySnapper = j;
                rectTransform.anchoredPosition = new Vector2(xSnapper, j);
                yield return new WaitForSeconds(delay);
            }
        }

    }
    IEnumerator Snap_X(Vector2 anchoredPos, RectTransform rectTransform)
    {
        float currX = rectTransform.anchoredPosition.x;
        if (anchoredPos.x > 0)
        {
            for (float i = currX; i < anchoredPos.x; i += snapSpeed)
            {
                xSnapper = i;
                rectTransform.anchoredPosition = new Vector2(i, ySnapper);
                yield return new WaitForSeconds(delay);
            }
        }
        else if (anchoredPos.x < 0)
        {
            for (float i = currX; i > anchoredPos.x; i -= snapSpeed)
            {
                xSnapper = i;
                rectTransform.anchoredPosition = new Vector2(i, ySnapper);
                yield return new WaitForSeconds(delay);
            }
        }

    }

    IEnumerator Zoom_IN(GameObject obj)
    {
        if (transform.localScale == maxScale)
            yield break;

        float currentScale = transform.localScale.x;
        for (float i = currentScale; i < maxZoomScale; i += zoomSpeed)
        {
            transform.localScale = new Vector3(i, i, i);
            yield return new WaitForSeconds(delay);
        }
        transform.localScale = maxScale;
        RectTransform rectTransform = GetRectTransform(obj);
        SnapTo_IN(rectTransform);
    }
    IEnumerator Zome_OUT(GameObject obj)
    {
        if (transform.localScale == initialScale)
            yield break;

        float currentScale = transform.localScale.x;
        for (float i = currentScale; i > minZoomScale; i -= zoomSpeed)
        {
            transform.localScale = new Vector3(i, i, i);
            yield return new WaitForSeconds(delay);
        }
        transform.localScale = initialScale;
        
        RectTransform rectTransform = GetRectTransform(obj);
        SnapTo_OUT(rectTransform);
        GetRectTransform(gameObject).anchoredPosition = initialAnchoredPos;
    }
    #endregion


    //Zooming with two fingrs.
    #region Zoom IN _ OUT _ Finger----------------------------------------------
    void ScrollZoom_IN_OUT()//Zoom IN_OUT with ScrollBar.
    {
        if (transform.localScale.x < minZoomScale || transform.localScale.y < minZoomScale)
        {
            transform.localScale = initialScale;
        }
        if (transform.localScale.x > maxZoomScale || transform.localScale.y > maxZoomScale)
        {
            transform.localScale = maxScale;
        }
        Slider slider = this.GetComponent<Slider>();
        currZoomScale = slider.value;
        currentScale = new Vector3(currZoomScale, currZoomScale, currZoomScale);
        transform.localScale = currentScale;
    }


    IEnumerator MaxZoomEffect(bool zoomMaxedOut)
    {
        yield return new WaitForSeconds(0.5f);
        if (zoomMaxedOut)
        {
            float currentScale = transform.localScale.x;
            for (float i = currentScale; i > maxZoomScale; i -= Time.deltaTime)
            {
                transform.localScale = new Vector3(i, i, i);
                yield return new WaitForSeconds(delay);
            }
            transform.localScale = maxScale;
        }
        else
        {

            float currentScale = transform.localScale.x;
            for (float i = currentScale; i < minZoomScale; i += Time.deltaTime)
            {
                transform.localScale = new Vector3(i, i, i);
                yield return new WaitForSeconds(delay);
            }
            transform.localScale = initialScale;
        }
        isMaxedOut = false;
        isMinedOut = false;
    }
    void PinchZoom()//Zoom IN_OUT with Pinch IN and OUT.
    {
        if (Input.touchCount > 1)
        {
            if (Input.touchCount != 2)
                return;
            Touch firstTouch = Input.GetTouch(0);
            Touch secondTouch = Input.GetTouch(1);
            if (firstTouch.phase != TouchPhase.Moved && secondTouch.phase != TouchPhase.Moved)
                return;
            scrollRect.enabled = false;
            firstTouchPrevPos = firstTouch.position - firstTouch.deltaPosition;
            secondTouchPrevPos = secondTouch.position - secondTouch.deltaPosition;

            touchesPrevPosDifference = (firstTouchPrevPos - secondTouchPrevPos).magnitude;
            touchesCurPosDifference = (firstTouch.position - secondTouch.position).magnitude;

            if (touchesPrevPosDifference > touchesCurPosDifference)
            {
                currZoomScale -= Time.deltaTime * 0.5f;
                if (IsInGrades(currZoomScale))
                    currentScale = new Vector3(currZoomScale, currZoomScale, currZoomScale);
                transform.localScale = currentScale;
                if (transform.localScale.x < 0.9f)
                {
                    currZoomScale = minZoomScale;
                    isMinedOut = true;
                    onlyOnce = true;
                    return;
                }
            }

            if (touchesPrevPosDifference < touchesCurPosDifference)
            {
                currZoomScale += Time.deltaTime * 0.5f;
                if (IsInGrades(currZoomScale))
                    currentScale = new Vector3(currZoomScale, currZoomScale, currZoomScale);
                transform.localScale = currentScale;
                if (transform.localScale.x > 2.1f)
                {
                    currZoomScale = maxZoomScale;
                    isMaxedOut = true;
                    onlyOnce = true;
                    return;
                }
            }

        }
        scrollRect.enabled = true;
    }

    float[] SetZoomGrades()//Setting ngrades for smoothing zooming process.
    {
        float[] grades = new float[12];
        grades[0] = 1f;
        for (int i = 0; i < grades.Length; i++)
        {
            grades[i] += 0.1f;
            if (i != grades.Length - 1)
            {
                grades[i + 1] = grades[i];
            }
        }
        return grades;
    }
    bool IsInGrades(float numb)//Is it in grades area?
    {
        for (int i = 0; i < zoomGrades.Length; i++)
        {
            if ((numb - zoomGrades[i]) < 0.01f)
            {
                return true;
            }
        }
        return false;
    }
    #endregion




    void LateUpdate()
    {
        if(isMinedOut == false && isMaxedOut == false && onlyOnce == false)
        {
            PinchZoom();
        }
        else if(isMinedOut && onlyOnce)
        {
            StartCoroutine(MaxZoomEffect(false));
            onlyOnce = false;
        }
        else if(isMaxedOut && onlyOnce)
        {
            StartCoroutine(MaxZoomEffect(true));
            onlyOnce = false;
        }
        

    }//Updateeeee
    #endregion
}//EndClassss