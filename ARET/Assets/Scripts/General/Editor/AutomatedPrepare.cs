﻿using UnityEditor;
using UnityEngine;

public class AutomatedPrepare 
{
    static string cbVersion = "1.0.0"; static int cbVersionCode = 1;
    static string gsVersion = "1.0.0"; static int gsVersionCode = 1;
    static string mkVersion = "1.0.0"; static int mkVersionCode = 1;
    static string iaVersion = "1.0.0"; static int iaVersionCode = 1;
    static string saVersion = "1.0.0";
    static string asVersion = "1.0.0";


    #region menu items
    [MenuItem("Build/Build Mode/Debug Build")]
    public static void DebugBuild() { GameData.CurrentBuildMode = BuildMode.debug; }
    [MenuItem("Build/Build Mode/Open Beta Build")]
    public static void OpenBetaBuild() { GameData.CurrentBuildMode = BuildMode.openBeta; }
    [MenuItem("Build/Build Mode/Release Build")]
    public static void ReleaseBuild() { GameData.CurrentBuildMode = BuildMode.release; }


    [MenuItem("Build/Debug/Switch For Android Debugging")]
    public static void PrepareForAndroidDebugging()
    {
        GameData.CurrentBuildMode = BuildMode.debug;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, "com.AAA.a.de.Words1100");
        SwitchPlatform(BuildTargetGroup.Android, BuildTarget.Android);
        SetSpecificAndroid();
        SetAllowedOrientations();
    }


    [MenuItem("Build/Debug/Switch For iOS Debugging")]
    public static void PrepareForiOSDebugging()
    {
        GameData.CurrentBuildMode = BuildMode.debug;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.iOS, "com.AAA.iOS.de.Words1100");
        SwitchPlatform(BuildTargetGroup.iOS, BuildTarget.iOS);
        SetSpecificiOS();
        SetAllowedOrientations();
    }






    [MenuItem("Build/Switch For Build/Cafe Bazaar")]
    public static void PrepareForCafeBazaar()
    {
        PlayerSettings.bundleVersion = cbVersion;
        PlayerSettings.Android.bundleVersionCode = cbVersionCode;
        GameData.CurrentStoreForBuild = Store.CafeBazaar;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, "com.AAA.cb.Words1100");
        SwitchPlatform(BuildTargetGroup.Android, BuildTarget.Android);
        SetSpecificAndroid();
        SetAllowedOrientations();
    }

    [MenuItem("Build/Switch For Build/Google Store")]
    public static void PrepareForGoogleStore()
    {
        PlayerSettings.bundleVersion = gsVersion;
        PlayerSettings.Android.bundleVersionCode = gsVersionCode;
        GameData.CurrentStoreForBuild = Store.GoogleStore;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, "com.AAA.gs.Words1100");
        SwitchPlatform(BuildTargetGroup.Android, BuildTarget.Android);
        SetSpecificAndroid();
        SetAllowedOrientations();
    }

    [MenuItem("Build/Switch For Build/Myket")]
    public static void PrepareForMyket()
    {
        PlayerSettings.bundleVersion = mkVersion;
        PlayerSettings.Android.bundleVersionCode = mkVersionCode;
        GameData.CurrentStoreForBuild = Store.Myket;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, "com.AAA.mk.Words1100");
        SwitchPlatform(BuildTargetGroup.Android, BuildTarget.Android);
        SetSpecificAndroid();
        SetAllowedOrientations();
    }

    [MenuItem("Build/Switch For Build/IranApps")]
    public static void PrepareForIranApps()
    {
        PlayerSettings.bundleVersion = iaVersion;
        PlayerSettings.Android.bundleVersionCode = iaVersionCode;
        GameData.CurrentStoreForBuild = Store.IranApps;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, "com.AAA.ia.Words1100");
        SwitchPlatform(BuildTargetGroup.Android, BuildTarget.Android);
        SetSpecificAndroid();
        SetAllowedOrientations();
    }

    [MenuItem("Build/Switch For Build/Sib App")]
    public static void PrepareForSibApp()
    {
        PlayerSettings.bundleVersion = saVersion;
        GameData.CurrentStoreForBuild = Store.SibApp;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.iOS, "com.AAA.sa.Words1100");
        SwitchPlatform(BuildTargetGroup.iOS, BuildTarget.iOS);
        SetSpecificiOS();
        SetAllowedOrientations();
    }

    [MenuItem("Build/Switch For Build/App Store")]
    public static void PrepareForAppStore()
    {
        PlayerSettings.bundleVersion = asVersion;
        GameData.CurrentStoreForBuild = Store.AppleStore;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.iOS, "com.AAA.as.Words1100");
        SwitchPlatform(BuildTargetGroup.iOS, BuildTarget.iOS);
        SetSpecificiOS();
        SetAllowedOrientations();
    }

    #endregion

    #region Shared


    public static void SwitchPlatform(BuildTargetGroup targetGroup, BuildTarget target)
    {
        EditorUserBuildSettings.SwitchActiveBuildTarget(targetGroup, target);
    }

    public static void SetAllowedOrientations()
    {
        Screen.orientation = ScreenOrientation.AutoRotation;
        Screen.autorotateToPortrait = true;
        Screen.autorotateToPortraitUpsideDown = true;
        Screen.autorotateToLandscapeLeft = false;
        Screen.autorotateToLandscapeRight = false;
    }

    public static void SetAppIcon(string iconName, BuildTargetGroup targetGroup)
    {
        // icon name is address like this: assets/ddd/xxxx.jpg
        Texture2D icon = Resources.Load(iconName) as Texture2D;
        Texture2D[] icons = new Texture2D[] { icon, icon, icon, icon };
        PlayerSettings.SetIconsForTargetGroup(targetGroup, icons);

    }


    static void SetSpecificAndroid()
    {
        //todo update all of these in right time
        GameData.CurrentPlatform = Platform.android;
        PlayerSettings.Android.minSdkVersion = AndroidSdkVersions.AndroidApiLevel24;
        PlayerSettings.Android.targetSdkVersion = AndroidSdkVersions.AndroidApiLevelAuto;
        PlayerSettings.Android.useCustomKeystore = false;
        PlayerSettings.Android.keystoreName = @"F:/OUTPUT/vania.keystore";
        PlayerSettings.Android.keystorePass = "vania123456";// keystore password
        PlayerSettings.Android.keyaliasName = "vania";
        PlayerSettings.Android.keyaliasPass = "vania123456";
        SetScreenOrientation();
    }
    static void SetSpecificiOS()
    {
        GameData.CurrentPlatform = Platform.ios;
        PlayerSettings.iOS.sdkVersion = iOSSdkVersion.DeviceSDK;
        SetScreenOrientation();
    }
    static void SetScreenOrientation()
    {
        Screen.autorotateToPortrait = true;
        Screen.autorotateToPortraitUpsideDown = true;
        Screen.autorotateToLandscapeLeft = false;
        Screen.autorotateToLandscapeRight = false;
        Screen.orientation = ScreenOrientation.AutoRotation;
    }



    #endregion

    #region Helper
    #endregion




}
