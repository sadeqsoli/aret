﻿using UnityEngine;

public static class DB
{
    #region Properties
    public static int LessonsLength { get { return 36; } }
    public static string FirstGift { get { return "ThisIsFirstgiftEveR"; } }
    public static string R_EX { get { return "R_EX"; } }
    public static string AR_EX { get { return "AR_EX"; } }
    public static string MAIN { get { return "MainMenu"; } }
    public static string AR_Lesson { get { return "AR_Lesson"; } }

    public static string Material { get { return @"General/mat/mat.mat"; } }


    #endregion

    #region Fields

    #endregion
    public static string Word_Repo(int lessonNumb)//TODO:NEED IMPROVMENT......
    {
        string wordRepo_Name = "";

        wordRepo_Name = "LearnedRepoName" + lessonNumb;

        //if there was no address return null.
        return wordRepo_Name;

    }
    public static string AR_Word_Repo(int lessonNumb)//TODO:NEED IMPROVMENT......
    {
        string wordRepo_Name = "";

        wordRepo_Name = "LearnedAugmentedRealityRepoName" + lessonNumb;

        //if there was no address return null.
        return wordRepo_Name;

    }


    public static string Paid_AR_Lessons(int lessonNumb)//TODO:NEED IMPROVMENT......
    {
        string lessonPaid_Repo = "lessonPaid" + lessonNumb + "_AugmentedReality";

        //if there was no address return null.
        return lessonPaid_Repo;

    }
    public static string PaidLessons(int lessonNumb)//TODO:NEED IMPROVMENT......
    {
        string lessonPaid_Repo = "lessonPaid" + lessonNumb + "_Normal";

        //if there was no address return null.
        return lessonPaid_Repo;

    }

    public static string LessonLength_W(int lessonNumb)//TODO:NEED IMPROVMENT......
    {
        string lessonLength_Repo = "lessonLength" + lessonNumb + "_Normal";

        //if there was no address return null.
        return lessonLength_Repo;

    }

    public static string LocalAPI(int lessonNumb)//TODO:NEED IMPROVMENT......
    {
        string localAPI_Address = Application.persistentDataPath + "/API/lesson" + lessonNumb + ".txt";

        //if there was no address return null.
        return localAPI_Address;

    }

    public static string ServerAPI(int lessonNumb)//TODO:NEED IMPROVMENT......
    {
        string serverAPI_Address = "http://www.sadeqsoli.ir/API/lesson" + lessonNumb + ".txt";

        //if there was no address return null.
        return serverAPI_Address;

    }

    public static string IsDownRepo(int lessonNumb)
    {
        string downloaded_RepoName = "";

        downloaded_RepoName = "Lessons_Images_downed" + lessonNumb;

        //if there was no address return null.
        return downloaded_RepoName;

    }
    public static string IMGLocalURL(int lessonNumb, int wordNumb)
    {
        string img_LocalURL = "";

        img_LocalURL = Application.persistentDataPath  + 
                                        "/DB/Lesson" + lessonNumb + "/" + wordNumb +".png";

        //if there was no address return null.
        return img_LocalURL;
    }
    public static string IMGServerURL(int lessonNumb, int wordNumb)
    {
        /*"http://sadeqsoli.ir/aret/IMG/L1/1.png"*/
        string img_ServerURL = ""; 

        img_ServerURL = "http://sadeqsoli.ir/" + "aret/IMG/L" + lessonNumb + "/" + wordNumb +".png";

        //if there was no address return null.
        return img_ServerURL;
    }
    public static string SceneDownPath()
    {
        string downloaded_RepoName = "";

        downloaded_RepoName = "PlotTaxi_L1_downed";

        //if there was no address return null.
        return downloaded_RepoName;
    }
    public static string SceneLoadPath()
    {
        string downloaded_RepoName = "";

        downloaded_RepoName = "PlotTaxi_L1_downed";

        //if there was no address return null.
        return downloaded_RepoName;
    }


}//EndClasssss

