﻿using Crosstales.RTVoice;
using UnityEngine;

public static class SpeakUp 
{

    public static void Speak(string msg,AudioSource audioSource, string speaker)
    {
        Speaker.Speak(msg, audioSource, Speaker.VoiceForName(speaker));
        //Speaker.Speak(msg, null, Speaker.VoiceForCulture())
    }

}
