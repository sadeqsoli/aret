﻿using UnityEngine;


public static class WordRepo
{
    #region Public Variables

    #endregion

    #region Private Variables
    static string fieldSeprator = "/";

    #endregion

    #region Public Methods
    public static void PopFromRepo(string oldInput, string repoKey)
    {
        if (!IsRepoHas(oldInput, repoKey))
            return;

        DeleteFromRepo(oldInput, repoKey);
    }
    public static void AddLearnedWords(string newInput, string repoKey)
    {
        if (IsRepoHas(newInput, repoKey))
            return;
        string allLearnedWords = Retrive(repoKey);
        allLearnedWords += newInput + fieldSeprator;
        Save(repoKey, allLearnedWords);
    }
    public static string GetRepo(string repoKey)
    {
        string repoTarget = Retrive(repoKey);
        int lastSlashW = Retrive(repoKey).LastIndexOf(fieldSeprator);
        if (repoTarget.Contains(fieldSeprator))
        {
            repoTarget = Retrive(repoKey).Remove(lastSlashW);
        }

        return repoTarget;
    }


    #endregion




    #region Private Methods

    static string[] RetriveFromRepoToArray(string repoKey)
    {
        string repoTarget = Retrive(repoKey);
        return repoTarget.Split('/');
    }

    static bool IsRepoHas(string NewInput, string repoKey)
    {
        string[] repoTarget = RetriveFromRepoToArray(repoKey);
        for (int i = 0; i < repoTarget.Length; i++)
        {
            if (repoTarget[i] == NewInput)
            {
                return true;
            }
        }
        return false;
    }

    static void DeleteFromRepo(string oldInput, string repoKey)
    {
        string[] Allwords = RetriveFromRepoToArray(repoKey);
        string value = Retrive(repoKey);
        for (int i = 0; i < Allwords.Length; i++)
        {
            if (Allwords[i] == oldInput)
            {
                Allwords[i] = "";
                value = ConvertToString(Allwords);
                if (!string.IsNullOrEmpty(value))
                    Save(repoKey, value);
            }
        }
    }

    static string ConvertToString(string[] str)
    {
        string newS = "";
        for (int i = 0; i < str.Length; i++)
        {
            if (!string.IsNullOrEmpty(str[i]))
            {
                newS += str[i] + fieldSeprator;
            }
        }
        return newS;
    }



    static string Retrive(string key)
    {
        return PlayerPrefs.GetString(key);
    }
    static void Save(string key, string val)
    {
        PlayerPrefs.SetString(key, val);
        PlayerPrefs.Save();
    }


    #endregion
}//EndClasssss


