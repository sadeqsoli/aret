﻿using UnityEngine;


public static class Lesson_Repo
{
    #region Properties
    #endregion

    #region Fields
    const string LESSON_NUMBER_REPO = "lesson_Number_Repo";
    const string CURRENT_LESSON_NUMBER_REPO = "current_Lesson_Number_Repo";

    const string AR_LESSON_NUMBER_REPO = "augmentedRealitylesson_Number_Repo";
    const string CURRENT_AR_LESSON_NUMBER_REPO = "current_AR_Lesson_Number_Repo";
    #endregion

    #region Public Methods
    public static void SaveCurrentLesson(int currentNewLesson)
    {
            Save(CURRENT_LESSON_NUMBER_REPO, currentNewLesson);
    }
    public static int GetCurrentLesson()
    {
        int currentLesson = Retrive(CURRENT_LESSON_NUMBER_REPO);
        return currentLesson;
    }



    public static void OpenNewLesson(int newLessonNumber)
    {
        int currentLesson = Retrive(LESSON_NUMBER_REPO);
        if (newLessonNumber > currentLesson)
            Save(LESSON_NUMBER_REPO, newLessonNumber);
    }

    public static int GetPassedLesson()
    {
        int currentLesson = Retrive(LESSON_NUMBER_REPO);
        return currentLesson;
    }




    public static void SaveCurrent_AR_Lesson(int currentNewLesson)
    {
        Save(CURRENT_AR_LESSON_NUMBER_REPO, currentNewLesson);
    }
    public static int GetCurrent_AR_Lesson()
    {
        int currentLesson = Retrive(CURRENT_AR_LESSON_NUMBER_REPO);
        return currentLesson;
    }

    public static void OpenNew_AR_Lesson(int newLessonNumber)
    {
        int currentLesson = Retrive(AR_LESSON_NUMBER_REPO);
        if (newLessonNumber > currentLesson)
            Save(AR_LESSON_NUMBER_REPO, newLessonNumber);
    }
    public static int GetPassed_AR_Lesson()
    {
        int currentAR_Lesson = Retrive(AR_LESSON_NUMBER_REPO);
        return currentAR_Lesson;
    }
    #endregion


    #region Private Methods
    static int Retrive(string key)
    {
        return PlayerPrefs.GetInt(key);
    }
    static void Save(string key, int val)
    {
        PlayerPrefs.SetInt(key, val);
    }
    #endregion
}//EndClasssss

public enum LessonNumber
{
    L1, L2, L3, L4, L5, L6, L7, L8, L9, L10,
    L11, L12, L13, L14, L15, L16, L17, L18, L19, L20,
    L21, L22, L23, L24, L25, L26, L27, L28, L29, L30,
    L31, L32, L33, L34, L35, L36, L37, L38, L39, L40,
    L41, L42, L43, L44, L45, L46, L47, L48
}
