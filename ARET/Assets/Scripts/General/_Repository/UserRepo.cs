﻿using UnityEngine;

public static class UserRepo
{
    #region Properties
    public static string RepoUser { get { return repoUser; } }

    #endregion

    #region Fields
    const string repoUser = "userRepo";
    const string repoPhone = "phoneRepo";
    const string repoEmail = "emailRepo";
    const string repoAuth = "Authrepo";
    const string repoRefreshAuth = "RefreshAuthrpo";

    #endregion

    #region Public Methods


    public static void PushUsername(string newUser)
    {
        Save(repoUser, newUser);
    }
    public static void PushPhone(string newPhone)
    {
        string phone = newPhone;
        Save(repoPhone, phone);
    }
    public static void PushEmail(string newEmail)
    {
        string email = newEmail;
        Save(repoEmail, email);
    }
    public static void PushAuth(string newAuth)
    {
        string authToken = newAuth;
        Save(repoAuth, authToken);
    }
    public static void PushRefreshAuth(string newRefreshAuth)
    {
        string refreshAuthToken = newRefreshAuth;
        Save(repoRefreshAuth, refreshAuthToken);
    }


    public static string GetUser()
    {
        return Retrive(repoUser);
    }
    public static string GetPhone()
    {
        return Retrive(repoPhone);
    }
    public static string GetEmail()
    {
        return Retrive(repoEmail);
    }
    public static string GetAuth()
    {
        return Retrive(repoAuth);
    }
    public static string GetRefreshAuth()
    {
        return Retrive(repoRefreshAuth);
    }

    #endregion




    #region Private Methods



    static string Retrive(string key)
    {
        return PlayerPrefs.GetString(key);
    }
    static void Save(string key, string val)
    {
        PlayerPrefs.SetString(key, val);
    }


    #endregion
}//EndClasssss
