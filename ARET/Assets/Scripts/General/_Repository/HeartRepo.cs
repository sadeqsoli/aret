﻿using UnityEngine;

public static class HeartRepo
{
    #region Properties
    #endregion

    #region Fields
    const string repo_Heart = "heartsRepo";
    #endregion

    #region Public Methods
    public static void PopHeart()
    {
        int allHearts = GetHeart();
        if (allHearts <= 0)
            return;
        if (!HasHeart(allHearts))
            return;

        allHearts -= 1;

        if (allHearts < 0)
        {
            allHearts = 0;
        }
        Save(repo_Heart, allHearts);
    }

    public static void PushHearts(int hearts)
    {
        int allHearts = GetHeart();
        allHearts += hearts;
        if (allHearts > 5)
        {
            allHearts = 5;
        }
        Save(repo_Heart, allHearts);
    }
    public static void AddHeart()
    {
        int allHearts = GetHeart();
        allHearts += 1;
        if (allHearts > 5)
        {
            allHearts = 5;
        }
        Save(repo_Heart, allHearts);
    }

    public static int GetHeart()
    {
        if (!PlayerPrefs.HasKey(repo_Heart))
        {
            return 0;
        }
        return Retrive(repo_Heart);
    }
    #endregion


    #region Private Methods

    static bool HasHeart(int reqHeart)
    {
        int allHearts = Retrive(repo_Heart);
        if (allHearts >= 1)
        {
            return true;
        }
        return false;
    }





    static int Retrive(string key)
    {
        return PlayerPrefs.GetInt(key);
    }
    static void Save(string key, int val)
    {
        PlayerPrefs.SetInt(key, val);
    }
    #endregion
}//EndClasssss


