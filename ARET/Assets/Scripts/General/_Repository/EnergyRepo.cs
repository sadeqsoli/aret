﻿using UnityEngine;

public static class EnergyRepo
{
    #region Properties
    #endregion

    #region Fields
    const string repo_EnergyBottle = "EnergyBottle_Repo";
    #endregion

    #region Public Methods
    public static void PopEnergyBottle()
    {
        int allEnergyBottles = GetEnergyBottles();
        if (allEnergyBottles <= 0)
            return;
        if (!HasEnergyBottles())
            return;

        allEnergyBottles -= 1;

        if (allEnergyBottles < 0)
        {
            allEnergyBottles = 0;
        }
        Save(repo_EnergyBottle, allEnergyBottles);
    }
    public static void PushEnergyBottle(int energyBottles)
    {
        int EnergyBottles = GetEnergyBottles();
        EnergyBottles += energyBottles;
        Save(repo_EnergyBottle, EnergyBottles);
    }
    public static void AddEnergyBottle()
    {
        int EnergyBottles = GetEnergyBottles();
        EnergyBottles += 1;
        Save(repo_EnergyBottle, EnergyBottles);
    }

    public static int GetEnergyBottles()
    {
        if (!PlayerPrefs.HasKey(repo_EnergyBottle))
        {
            return 0;
        }
        return Retrive(repo_EnergyBottle);
    }
    #endregion


    #region Private Methods

    static bool HasEnergyBottles()
    {
        int EnergyBottles = Retrive(repo_EnergyBottle);
        if (EnergyBottles >= 1)
        {
            return true;
        }
        return false;
    }





    static int Retrive(string key)
    {
        return PlayerPrefs.GetInt(key);
    }
    static void Save(string key, int val)
    {
        PlayerPrefs.SetInt(key, val);
    }
    #endregion
}//EndClasssss

