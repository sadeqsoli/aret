﻿using UnityEngine;

public static class AppleRepo
{
    #region Properties
    #endregion

    #region Fields
    const string repo_apple = "apples_Repo";
    #endregion

    #region Public Methods
    public static bool HasApples(int reqApples)
    {
        int Apples = GetApples();
        if (Apples >= reqApples)
        {
            return true;
        }
        return false;
    }
    public static void PopApple(int reqApples)
    {
        int allApples = GetApples();
        if (allApples <= 0)
            return;
        allApples -= reqApples;

        if (allApples < 0)
        {
            allApples = 0;
        }
        Save(repo_apple, allApples);
    }

    public static void PushApple(int apples)
    {
        int allApples = GetApples();
        allApples += apples;

        Save(repo_apple, allApples);
    }
    public static void AddApple()
    {
        int allApples = GetApples();
        allApples += 1;
        if (allApples > 5)
        {
            allApples = 5;
        }
        Save(repo_apple, allApples);
    }

    public static int GetApples()
    {
        if (!PlayerPrefs.HasKey(repo_apple))
        {
            return 0;
        }
        return Retrive(repo_apple);
    }
    #endregion


    #region Private Methods







    static int Retrive(string key)
    {
        return PlayerPrefs.GetInt(key);
    }
    static void Save(string key, int val)
    {
        PlayerPrefs.SetInt(key, val);
    }
    #endregion
}//EndClasssss


