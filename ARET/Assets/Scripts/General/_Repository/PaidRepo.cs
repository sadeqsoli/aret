﻿using UnityEngine;

public static class PaidRepo
{
    #region Properties
    #endregion

    #region Fields
    #endregion

    #region Public Methods


    public static void SetPaidLesson(int ZO_Check, string repoKey)
    {
        if (ZO_Check == 0 || ZO_Check == 1)
            Save(repoKey, ZO_Check);
    }
    public static void SetPaid_AR_Lesson(int ZO_Check, string repoKey)
    {
        if (ZO_Check == 0 || ZO_Check == 1)
            Save(repoKey, ZO_Check);
    }


    public static int GetPaid_AR_Lessons(string repoKey)
    {
        if (!PlayerPrefs.HasKey(repoKey))
        {
            return 0;
        }
        return Retrive(repoKey);
    }
    public static int GetPaidLessons(string repoKey)
    {
        if (!PlayerPrefs.HasKey(repoKey))
        {
            return 0;
        }
        return Retrive(repoKey);
    }
    #endregion


    #region Private Methods







    static int Retrive(string key)
    {
        return PlayerPrefs.GetInt(key);
    }
    static void Save(string key, int val)
    {
        PlayerPrefs.SetInt(key, val);
    }
    #endregion
}//EndClasssss
