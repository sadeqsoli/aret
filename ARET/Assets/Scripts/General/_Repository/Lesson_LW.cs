﻿using UnityEngine;

public static class Lesson_LW
{
    #region Properties
    #endregion

    #region Fields
    #endregion

    #region Public Methods


    public static void SetWordsLength(int wordsLength ,string repoKey)
    {

        Save(repoKey, wordsLength);
    }


    public static int GetWordsLength(string repoKey)
    {
        if (!PlayerPrefs.HasKey(repoKey))
        {
            return 0;
        }
        return Retrive(repoKey);
    }
    #endregion


    #region Private Methods







    static int Retrive(string key)
    {
        return PlayerPrefs.GetInt(key);
    }
    static void Save(string key, int val)
    {
        PlayerPrefs.SetInt(key, val);
    }
    #endregion
}//EndClasssss



