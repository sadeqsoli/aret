﻿using UnityEngine;

public static class TempRepo
{
    #region Properties
    public static string RepoTemp { get { return repoTemp; } }

    #endregion

    #region Fields
    const string repoTemp = "TempWord_Repo";

    #endregion

    #region Public Methods


    public static void PushTempWord(string newWord)
    {
        Save(repoTemp, newWord);
    }



    public static string GetTempWord()
    {
        return Retrive(repoTemp);
    }

    #endregion




    #region Private Methods



    static string Retrive(string key)
    {
        return PlayerPrefs.GetString(key);
    }
    static void Save(string key, string val)
    {
        PlayerPrefs.SetString(key, val);
    }


    #endregion
}//EndClasssss
