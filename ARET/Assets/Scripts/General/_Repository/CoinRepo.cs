﻿using UnityEngine;

public static class CoinRepo
{
    #region Properties
    #endregion

    #region Fields
    const string repo_Coin = "coinsRepo";
    #endregion

    #region Public Methods
    public static bool HasCoin(int reqCoins)
    {
        int allCoins = GetCoin();
        if (allCoins >= reqCoins)
        {
            return true;
        }
        return false;
    }
    public static void PopCoins(int reqCoins)
    {
        if (reqCoins <= 0)
            return;
        int allCoins = GetCoin();
        allCoins -= reqCoins;

        Save(repo_Coin, allCoins);
    }
    public static void PushCoins(int newCoins)
    {
        int allCoins = GetCoin();
        allCoins += newCoins;

        Save(repo_Coin, allCoins);
    }
    public static int GetCoin()
    {
        if (!PlayerPrefs.HasKey(repo_Coin))
        {
            return 0;
        }
        return Retrive(repo_Coin);
    }
    #endregion


    #region Private Methods







    static int Retrive(string key)
    {
        return PlayerPrefs.GetInt(key);
    }
    static void Save(string key, int val)
    {
        PlayerPrefs.SetInt(key, val);
    }
    #endregion
}//EndClasssss


