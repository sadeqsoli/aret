﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using TMPro;

public class HostBundle : MonoBehaviour
{
    #region public Variables
    //Static Instance of HostBundle Class.
    public static HostBundle Instance = new HostBundle();
    TextMeshProUGUI txtstatus;
    [SerializeField] GameObject panel;

    #endregion

    #region Private Variables

    #endregion

    #region Public Methods
    public void Asset_Loader(string Asseturl, string localUrl, string saveName)
    {
        if (File.Exists(Application.persistentDataPath + "/" + "" + "/Asset"))
        {
            StartCoroutine(LoadAsset());
        }
        else
        {
            StartCoroutine(Download_Write(Asseturl, localUrl, saveName));
        }
    }

    public void Scene_Loader(string downloadUrl, string localUrl, string saveName)
    {
        if (!File.Exists(localUrl + saveName))
        {
            StartCoroutine(Download_Write(downloadUrl, localUrl, saveName));
        }
        else
        {
            StartCoroutine(Load_Scene(localUrl, saveName));
        }
    }


    public void Delete(string DlAddress)
    {
        if (File.Exists(Application.persistentDataPath + "/" + "" + "/" + DlAddress))
        {
            File.Delete(Application.persistentDataPath + "/" + "" + "/" + DlAddress);
            print("Deleted " + Application.persistentDataPath + "/" + "" + "/" + DlAddress);
            txtstatus.text = "Deleted " + Application.persistentDataPath + "/" + "" + "/" + DlAddress;
        }
        else
            print("Cant delete");
        txtstatus.text = "Cant delete " + Application.persistentDataPath + "/" + "" + "/" + DlAddress;
    }



    #endregion


    #region Private Methods
    void Awake()
    {
        panel.SetActive(false);
        Instance = this;
    }//Startttttt

    IEnumerator LoadAsset()
    {
        UnityWebRequest request = UnityWebRequestAssetBundle.GetAssetBundle("file:///" + Application.persistentDataPath + "/" + "" + "/Asset");
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
            txtstatus.text = request.error;
        }
        else
        {
            print("Loading");
            txtstatus.text = "Loading";
            AssetBundle asset = DownloadHandlerAssetBundle.GetContent(request);
            string[] All_Names = asset.GetAllAssetNames();
            foreach (string name in All_Names)
            {
                if (name != null)
                {
                    GameObject gameAsset = (GameObject)asset.LoadAsset(name);
                    Instantiate(gameAsset);
                    print("ObjectName= " + name);
                    txtstatus.text = "ObjectName= " + name;
                }

            }
        }
        print("Loaded");
        txtstatus.text = "Loaded";
    }


    
    IEnumerator Load_Scene(string localUrl, string saveName)
    {
        panel.SetActive(true);
        UnityWebRequest request = UnityWebRequestAssetBundle.GetAssetBundle("file://" + localUrl + saveName);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
        }
        else
        {
            AssetBundle asset = DownloadHandlerAssetBundle.GetContent(request);
            string[] scenes = asset.GetAllScenePaths();
            foreach (string scenename in scenes)
            {
                print("scene= " + scenename);
                print("scene= " + saveName);
                txtstatus.text = "scene= " + scenename;
                H_SceneManager.GoToSpecificScene(scenename);
            }
        }
        panel.SetActive(false);
    }

    IEnumerator Download_Write(string DownloadURL, string localUrl, string saveName)
    {
        panel.SetActive(true);
        UnityWebRequest request = UnityWebRequest.Get(DownloadURL);

        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
        }
        else
        {
            //Create Directory
            if (!Directory.Exists(localUrl))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(localUrl));
            }
            //Save Assets
            File.WriteAllBytes(localUrl + saveName, request.downloadHandler.data);
        }
        print("Download");
        panel.SetActive(false);

    }


    void Update()
    {


    }//Updateeeee
    #endregion
}//EndClasssss
