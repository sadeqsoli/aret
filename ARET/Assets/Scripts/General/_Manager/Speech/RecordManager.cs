﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;
using KKSpeech;

public class RecordManager : MonoBehaviour
{
    #region Properties
    public static string Result { get { return daResult; } }
    #endregion

    #region Fields
    static string daResult;
    #endregion

    #region Public Methods
    public void OnFinalResult(string result)
    {
        daResult = result;
    }

    public void OnPartialResult(string result)
    {
        daResult = result;
    }

    public void OnAvailabilityChange(bool available)
    {
        //startRecordingButton.enabled = available;
        if (!available)
        {
            Debug.Log("Speech Recognition not available");
            //statusTXT.text = "Speech Recognition not available";
        }
        else
        {
            Debug.Log("Say something :-)");
            //statusTXT.text = "Say something :-)";
        }
    }

    public void OnAuthorizationStatusFetched(AuthorizationStatus status)
    {
        switch (status)
        {
            case AuthorizationStatus.Authorized:
                //startRecordingButton.enabled = true;
                Debug.Log("Ready to talk... " + status);
                //statusTXT.text = "Ready to talk... ";
                break;
            default:
                //startRecordingButton.enabled = false;
                Debug.LogWarning("Cannot use Speech Recognition, authorization status is " + status);
                break;
        }
    }

    //public void OnEndOfSpeech()
    //{
    //    Debug.Log("End of recording");
    //    statusTXT.text = "End of recording";
    //}

    public void OnError(string error)
    {
        Debug.LogError(error);
    }

    public void StopRecording()
    {
        SpeechRecognizer.StopIfRecording();
        Microphone.End(Microphone.devices[0]);
        GetSpeechPercent.GetResult(daResult);
        Debug.Log("End of recording");
    }


    public void OnStartRecordingPressed()
    {
        if (SpeechRecognizer.IsRecording())
        {
            SpeechRecognizer.StopIfRecording();
            Microphone.End(Microphone.devices[0]);
        }
        else
        {
            SpeechRecognizer.StartRecording(true);
            Debug.Log("Recording...");
        }
    }
    #endregion

    #region Private Methods
    void Start()
    {
        //StopRecording();
        if (SpeechRecognizer.ExistsOnDevice())
        {
            SpeechRecognizerListener listener = GameObject.FindObjectOfType<SpeechRecognizerListener>();
            listener.onAuthorizationStatusFetched.AddListener(OnAuthorizationStatusFetched);
            listener.onAvailabilityChanged.AddListener(OnAvailabilityChange);
            listener.onErrorDuringRecording.AddListener(OnError);
            listener.onErrorOnStartRecording.AddListener(OnError);
            listener.onFinalResults.AddListener(OnFinalResult);
            listener.onPartialResults.AddListener(OnPartialResult);
            //listener.onEndOfSpeech.AddListener(OnEndOfSpeech);
            SpeechRecognizer.RequestAccess();
        }
        else
        {
            //Debug.LogWarning("Sorry, but this device doesn't support speech recognition");
            //statusTXT.text = "Sorry, but this device doesn't support speech recognition";

        }

    }//Startttttt








    void Update()
    {


    }//Updateeeee
    #endregion
}//EndClassss
