﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;


public enum HardnessDegree
{
    easy,
    medium,
    hard,
    superHard
}
public static class GetSpeechPercent
{
    #region Properties
    public static bool IsCorrect { get { return isCorrect; } }
    public static HardnessDegree HardDegree { get; set; }
    public static float Hardness { get { return hardness; } }

    #endregion

    #region Private Variables
    static float hardness;
    static string currentTalk;
    static float percentage;
    static bool isCorrect;
    #endregion

    #region Public Methods

    public static float CorrectnessPercent()
    {
        return percentage;
    }
    public static void CurrentTalk(string currentconv)
    {
        currentTalk = STR.RemoveMarks(currentconv);
    }
    public static void GetResult(string str)
    {
        if (String.IsNullOrEmpty(str))
        {

        }
        else
        {
            IsCorrect_WS(str);
        }
    }
    #endregion


    #region Private Methods

    static void IsCorrect_WS(string strTXT)
    {
        string CurrentSource = "";
        string finalSpeech = "";
        string[] ct = currentTalk.Split(' ');
        for (int i = 0; i < ct.Length; i++)
        {
            CurrentSource += ct[i];
        }
        string[] userSpeech = strTXT.Split(' ');
        for (int i = 0; i < userSpeech.Length; i++)
        {
           string userStr = STR.RemoveMarks(userSpeech[i]);
           finalSpeech += userStr;
        }
        float correctness = Calculator.CalculatePro(finalSpeech, CurrentSource);
        percentage = correctness;

        if (correctness >= 70)
        {
            isCorrect = true;
        }
        else
        {
            isCorrect = false;
        }
    }

    static float CorrectNumber(HardnessDegree hardnessDegree)
    {
        if (hardnessDegree == HardnessDegree.easy)
        {
            return hardness = 70f;
        }
        else if (hardnessDegree == HardnessDegree.medium)
        {
            return hardness = 80f;
        }
        else if (hardnessDegree == HardnessDegree.hard)
        {
            return hardness = 90f;
        }
        else if (hardnessDegree == HardnessDegree.superHard)
        {
            return hardness = 99f;
        }
        return 0f;
    }



    #endregion
}//EndClasssss
