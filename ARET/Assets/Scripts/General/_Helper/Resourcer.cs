﻿using System.IO;
using UnityEngine;
using UnityEngine.Video;


public static class Resourcer
{
    #region Properties
    static string gif_FILE_EXTENSION { get { return GIF_FILE_EXTENSION; } }
    static string png_FILE_EXTENSION { get { return PNG_FILE_EXTENSION; } }
    static string mp3_FILE_EXTENSION { get { return MP3_FILE_EXTENSION; } }
    static string wav_FILE_EXTENSION { get { return WAV_FILE_EXTENSION; } }
    static string text_FILE_EXTENSION { get { return TEXT_FILE_EXTENSION; } }
    static string mat_FILE_EXTENSION { get { return MAT_FILE_EXTENSION; } }
    #endregion

    #region Fields
    static string GIF_FILE_EXTENSION = @".gif";
    static string PNG_FILE_EXTENSION = @".png";
    static string MP3_FILE_EXTENSION = @".mp3";
    static string WAV_FILE_EXTENSION = @".wav";
    static string TEXT_FILE_EXTENSION = @".json";
    static string MAT_FILE_EXTENSION = @".mat";
    #endregion


    #region Public Methods
    public static Material MatLoader(string path)
    {
        return ReturnMaterialResource(path);
    }
    public static string TextLoader(string path)
    {
        return ReturnTextResource(path);
    }
    public static Sprite SpriteLoader(string path)
    {
        return ReturnSpriteResource(path);
    }
    public static VideoClip VideoLoader(string path)
    {
        return ReturnVideoResource(path);
    }
    public static AudioClip Mp3Loader(string path)
    {
        return ReturnSoundResource(path);
    }
    public static AudioClip WAVLoader(string path)
    {
        return ReturnWavResource(path);
    }
    #endregion

    #region Private Methods

    /// <summary>
    /// Removes the default file extension from path.
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    static string RemoveFileExtension(string path, string fileExtension)
    {
        string _FILE_EXTENSION = fileExtension;
        if (path.Length >= _FILE_EXTENSION.Length)
        {
            //If file extension exist, remove it.
            if (path.ToLower().Substring(path.Length - _FILE_EXTENSION.Length, _FILE_EXTENSION.Length) == _FILE_EXTENSION.ToLower())
                return path.Substring(0, path.Length - _FILE_EXTENSION.Length);
            //File extension doesn't exist.
            else
                return path;
        }
        //Path isn't long enough to contain file extension.
        else
        {
            return path;
        }
    }

    /// <summary>
    /// Removes the directory separator if at the begining of path.
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    static string RemoveLeadingDirectorySeparator(string path)
    {
        //Remove directory separate character if it exist on the first character.
        if (char.Parse(path.Substring(0, 1)) == Path.DirectorySeparatorChar || char.Parse(path.Substring(0, 1)) == Path.AltDirectorySeparatorChar)
            return path.Substring(1);
        else
            return path;
    }


    /// <summary>
    /// Returns string result of a text file from Resources.
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    static string ReturnTextResource(string path)
    {
        //Remove default file extension and format the path to the platform.
        path = RemoveFileExtension(path, TEXT_FILE_EXTENSION);
        path = RemoveLeadingDirectorySeparator(path);

        if (path == string.Empty)
        {
            Debug.LogError("ReturnFileResource -> path is empty.");
            return string.Empty;
        }

        //Try to load text from file path.
        TextAsset textAsset = Resources.Load(path) as TextAsset;

        if (textAsset != null)
            return textAsset.text;
        else
            return string.Empty;
    }



    /// <summary>
    /// Returns Sprite result of a text address from Resources.
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    static Material ReturnMaterialResource(string path)
    {
        if (path.EndsWith(MAT_FILE_EXTENSION))
        {
            //Remove default file extension and format the path to the platform.
            path = RemoveFileExtension(path, PNG_FILE_EXTENSION);
            path = RemoveLeadingDirectorySeparator(path);
        }
        if (path == string.Empty)
        {
            Debug.LogError("ReturnFileResource -> path is empty.");
            return null;
        }
        //Try to load sprite from file path.
        Material materialAsset = Resources.Load<Material>(path) as Material;

        if (materialAsset != null)
            return materialAsset;
        else
            return null;


    }

    /// <summary>
    /// Returns Sprite result of a text address from Resources.
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    static Sprite ReturnSpriteResource(string path)
    {
        if (path.EndsWith(PNG_FILE_EXTENSION))
        {
            //Remove default file extension and format the path to the platform.
            path = RemoveFileExtension(path, PNG_FILE_EXTENSION);
            path = RemoveLeadingDirectorySeparator(path);
        }
        if (path == string.Empty)
        {
            Debug.LogError("ReturnFileResource -> path is empty.");
            return null;
        }
        //Try to load sprite from file path.
        Sprite spriteAsset = Resources.Load<Sprite>(path) as Sprite;

        if (spriteAsset != null)
            return spriteAsset;
        else
            return null;


    }

    /// <summary>
    /// Returns video Clip result of a text address from Resources.
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    static VideoClip ReturnVideoResource(string path)
    {
        if (path.EndsWith(GIF_FILE_EXTENSION))
        {
            //Remove default file extension and format the path to the platform.
            path = RemoveFileExtension(path, GIF_FILE_EXTENSION);
            path = RemoveLeadingDirectorySeparator(path);
        }

        if (path == string.Empty)
        {
            Debug.LogError("ReturnFileResource -> path is empty.");
            return null;
        }
        //Try to load videoClip from file path.
        VideoClip videoAsset = Resources.Load<VideoClip>(path) as VideoClip;

        if (videoAsset != null)
            return videoAsset;
        else
            return null;

    }




    /// <summary>
    /// Returns string result of a text file from Resources.
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    static AudioClip ReturnSoundResource(string path)
    {
        //Remove default file extension and format the path to the platform.
        path = RemoveFileExtension(path, MP3_FILE_EXTENSION);
        path = RemoveLeadingDirectorySeparator(path);

        if (path == string.Empty)
        {
            Debug.LogError("ReturnFileResource -> path is empty.");
            return null;
        }

        //Try to load AudioClip from file path.
        AudioClip audioAsset = Resources.Load(path) as AudioClip;

        if (audioAsset != null)
            return audioAsset;
        else
            return null;
    }


    /// <summary>
    /// Returns string result of a text file from Resources.
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    static AudioClip ReturnWavResource(string path)
    {
        //Remove default file extension and format the path to the platform.
        path = RemoveFileExtension(path, WAV_FILE_EXTENSION);
        path = RemoveLeadingDirectorySeparator(path);

        if (path == string.Empty)
        {
            Debug.LogError("ReturnFileResource -> path is empty.");
            return null;
        }

        //Try to load AudioClip from file path.
        AudioClip audioAsset = Resources.Load(path) as AudioClip;

        if (audioAsset != null)
            return audioAsset;
        else
            return null;
    }

    #endregion


}//EndClasssssss
