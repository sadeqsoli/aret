using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;


public class UnityEventString : UnityEvent<string> { }
public class UnityEventBool : UnityEvent<bool> { }

public class EventManager : Singleton<EventManager>
{
    public static bool IsEvent_Initialized { get; private set; } = false;
    Dictionary<string, UnityEvent> eventDictionary = new Dictionary<string, UnityEvent>();
    Dictionary<string, UnityEvent<string>> stringEventDictionary = new Dictionary<string, UnityEvent<string>>();
    Dictionary<string, UnityEvent<bool>> boolEventDictionary = new Dictionary<string, UnityEvent<bool>>();

    protected override void Awake()
    {
        base.Awake();
        Initialize();
        //AddEvents();
    }

    void Initialize()
    {
        if (eventDictionary == null)
            eventDictionary = new Dictionary<string, UnityEvent>();
        if (stringEventDictionary == null)
            stringEventDictionary = new Dictionary<string, UnityEvent<string>>();
        if (boolEventDictionary == null)
            boolEventDictionary = new Dictionary<string, UnityEvent<bool>>();
        IsEvent_Initialized = true;
    }

    void AddEvents()
    {
        if (IsEvent_Initialized)
        {
            AddStringEvent(PEM.PronounWord_N);
        }
    }
   





    #region No Parameter Events
    public static void AddEvent(string eventName)
    {
        foreach (KeyValuePair<string, UnityEvent> item in Instance.eventDictionary)
        {
            if (item.Key == eventName)
                return;
        }
        UnityEvent newEvent = new UnityEvent();
        Instance.eventDictionary.Add(eventName, newEvent);
    }

    public static void StartListening(string eventName, UnityAction listener)
    {
        UnityEvent thisEvent;
        if (Instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.AddListener(listener);
        }
        else
        {
            thisEvent = new UnityEvent();
            thisEvent.AddListener(listener);
            Instance.eventDictionary.Add(eventName, thisEvent);
        }
    }

    public static void StopListening(string eventName, UnityAction listener)
    {
        UnityEvent thisEvent;
        if (Instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.RemoveListener(listener);
        }
    }

    public static void TriggerEvent(string eventName)
    {
        UnityEvent thisEvent;
        if (Instance.eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            if (thisEvent != null)
            {
                Debug.Log(eventName + "'s Event is not null!");
                thisEvent.Invoke();
            }
            else
            {
                Debug.Log(eventName + "'s Event is null!");
            }
        }
    }
    #endregion


    #region  Events with one String parameter

    public static void AddStringEvent(string eventName)
    {
        foreach (KeyValuePair<string, UnityEvent<string>> item in Instance.stringEventDictionary)
        {
            if (item.Key == eventName)
                return;
        }
        UnityEvent<string> newEvent = new UnityEventString();
        Instance.stringEventDictionary.Add(eventName, newEvent);
    }
    public static void StartListening(string eventName, UnityAction<string> listener)
    {
        UnityEvent<string> thisEvent;
        if (Instance.stringEventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.AddListener(listener);
        }
        else
        {
            thisEvent = new UnityEventString();
            thisEvent.AddListener(listener);
            Instance.stringEventDictionary.Add(eventName, thisEvent);
        }
    }

    public static void StopListening(string eventName, UnityAction<string> listener)
    {
        UnityEvent<string> thisEvent;
        if (Instance.stringEventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.RemoveListener(listener);
        }
    }

    public static void TriggerEvent(string eventName, string targetString)
    {
        Debug.Log(targetString);
        UnityEvent<string> thisEvent;
        if (Instance.stringEventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent?.Invoke(targetString);
            Debug.Log(eventName + "'s Event is not null!");
        }
    }
    #endregion


    #region  Events with one Bool parameter

    public static void AddBoolEvent(string eventName)
    {
        foreach (KeyValuePair<string, UnityEvent<bool>> item in Instance.boolEventDictionary)
        {
            if (item.Key == eventName)
                return;
        }
        UnityEvent<bool> newEvent = new UnityEventBool();
        Instance.boolEventDictionary.Add(eventName, newEvent);
    }
    public static void StartListening(string eventName, UnityAction<bool> listener)
    {
        UnityEvent<bool> thisEvent;
        if (Instance.boolEventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.AddListener(listener);
        }
        else
        {
            thisEvent = new UnityEventBool();
            thisEvent.AddListener(listener);
            Instance.boolEventDictionary.Add(eventName, thisEvent);
        }
    }

    public static void StopListening(string eventName, UnityAction<bool> listener)
    {
        UnityEvent<bool> thisEvent;
        if (Instance.boolEventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.RemoveListener(listener);
        }
    }

    public static void TriggerEvent(string eventName, bool isTrue)
    {
        Debug.Log(isTrue);
        UnityEvent<bool> thisEvent;
        if (Instance.boolEventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent?.Invoke(isTrue);
            Debug.Log(eventName + "'s Event is not null!");
        }
    }
    #endregion

}//EndClassss


public static class PEM
{
    //Events For Sending strings to the 
    public static string  PronounWord_N { get { return "PronounWord_N_Event"; } }
 
}

