﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class H_SceneManager
{
    static int TapCount = 0;
    static float NewTime;
    static float MaxDubbleTapTime = 0.3f;
    public static bool GetBackByEscapeButton()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            // Check if Back was pressed this frame
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                return true;
            }
        }
        return false;
    }

    public static void GoToNextOrPrevScene(bool isGoingNext)
    {
        int buildIndex = SceneManager.GetActiveScene().buildIndex;
        int buildNumber;
        if (isGoingNext)
        {
            buildNumber = buildIndex + 1;
        }
        else
        {
            buildNumber = buildIndex - 1;
        }
        SceneManager.LoadScene(buildNumber);
    }
    public static void GoToSpecificScene(int buildIndex)
    {
        SceneManager.LoadScene(buildIndex);
    }
    public static void GoToSpecificScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
    public static void ResetScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public static void QuitByEscapeButton()
    {
        if (Application.platform == RuntimePlatform.Android)
        {

            // Check if Back was pressed this frame
            if (GetBackByEscapeButton())
            {
                TapCount += 1;
                if (TapCount == 1)
                {
                    NewTime = Time.time + MaxDubbleTapTime;
                }
                else if (TapCount == 2 && Time.time <= NewTime)
                {
                    // Quit the application
                    Application.Quit(0);
                }
            }
        }
    }



}//EndCalssss
 //AndroidJavaObject activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
 //activity.Call<bool>("moveTaskToBack", true);