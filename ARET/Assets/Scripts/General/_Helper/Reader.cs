﻿using System.IO;
using UnityEngine;
using System.Collections.Generic;
using System.Text;
using UnityEngine.Networking;
using System.Collections;

public class Reader : Singleton<Reader>
{

    #region Properties
    public int WordsLength { get { return wordsLength; } }
    public int QuestionLength { get { return questionLength; } }
    public int AnswerLength { get { return answerLength; } }

    public string[,] Words { get { return words_Matrix; } }
    public string[,] Questions { get { return question_Matrix; } }
    public string[,] Answers { get { return answer_Matrix; } }
    public string[] Tilte { get { return titles; } }

    #endregion


    #region Fields
    const char lineSeperater = '\n'; // It defines line seperate character
    const char fieldSeperator = '*'; // It defines field seperate chracter
    const string FILE_EXTENSION = @".txt"; // Extension of the database files.

    //[SerializeField] GameObject downPanel;

    int wordsLength;
    int questionLength;
    int answerLength;

    string[,] words_Matrix;
    string[,] question_Matrix;
    string[,] answer_Matrix;
    string[] titles;

    string[] lessonss;
    [SerializeField] TextAsset[] lessons;


    #endregion

    #region Private Methods
    void Start()
    {

    }


    //IEnumerator LoadingAllOfLessons()
    //{
    //    for (int i = 0; i < DB.LessonsLength; i++)
    //    {
    //        string localURL = DB.LocalAPI(i + 1);
    //        string serverURL = DB.ServerAPI(i + 1);
    //        if (File.Exists(localURL))
    //        {
    //            //lessons[i] = HelperResourcer.TextLoader(localURL);
    //        }
    //        else
    //        {
    //            downPanel.SetActive(true);
    //            yield return DownloadFile(localURL, serverURL, i);
    //        }
    //    }
    //    downPanel.SetActive(false);
    //}







    void UpdateMatrix(int wordNumb, int questionNumb, int answerNumb)
    {
        wordsLength = wordNumb;
        questionLength = questionNumb;
        answerLength = answerNumb;

        words_Matrix = new string[0, 0];
        words_Matrix = new string[wordNumb, wordNumb];

        question_Matrix = new string[0, 0];
        question_Matrix = new string[questionNumb, questionNumb];

        answer_Matrix‌ = new string[0, 0];
        answer_Matrix‌ = new string[answerNumb, answerNumb];
    }


    public void ReadCurrentLesson(int lessonNumber)
    {
        string[] lines;
        string[] fields;
        int wordNumb = 0; int w_Numb = 0;
        int questionNumb = 0; int q_Numb = 0;
        int answerNumb = 0; int a_Numb = 0;
        int titleNumb = 0; int t_Numb = 0;

        lines = lessons[lessonNumber - 1].text.Split(lineSeperater);

        for (int i = 0; i < lines.Length; i++)
        {
            fields = lines[i].Split(fieldSeperator);
            if (fields[0] == "w")
            {
                wordNumb++;
            }
            else if (fields[0] == "q")
            {
                questionNumb++;
            }
            else if (fields[0] == "a")
            {
                answerNumb++;
            }
            else if (fields[0] == "t")
            {
                titleNumb++;
            }
        }
        //initializing Matrixs.
        UpdateMatrix(wordNumb, questionNumb, answerNumb);

        for (int lineNumb = 0; lineNumb < lines.Length; lineNumb++)
        {
            fields = lines[lineNumb].Split(fieldSeperator);
            if (fields[0] == "w")
            {
                for (int fieldNumb = 1; fieldNumb < fields.Length; fieldNumb++)
                {
                    words_Matrix[w_Numb, fieldNumb - 1] = fields[fieldNumb];
                }
                w_Numb++;
            }
            if (fields[0] == "q")
            {
                for (int fieldNumb = 1; fieldNumb < fields.Length; fieldNumb++)
                {
                    question_Matrix[q_Numb, fieldNumb - 1] = fields[fieldNumb];
                }
                q_Numb++;
            }
            //if (fields[0] == "a")
            //{
            //    for (int fieldNumb = 1; fieldNumb < fields.Length; fieldNumb++)
            //    {
            //        answer_Matrix[a_Numb, fieldNumb - 1] = fields[fieldNumb];
            //    }
            //    a_Numb++;
            //}
            if (fields[0] == "t")
            {
                titles = new string[fields.Length - 1];
                for (int fieldNumb = 1; fieldNumb < fields.Length; fieldNumb++)
                {
                    titles[t_Numb] = fields[fieldNumb];
                    t_Numb++;
                }

            }
        }
    }





    void Update()
    {

    }
    #endregion
}//EndClasss

