﻿using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Events;
using TMPro;
using System.Collections;

public class MainMenuManager : MonoBehaviour
{
    #region Properties

    #endregion

    #region Fields 
    [SerializeField] GameObject _InformPanel;
    [SerializeField] TextMeshProUGUI heartTXT, coinTXT, energyTXT, appleTXT;
    [SerializeField] Image heartIMG, coinIMG, energyIMG, appleIMG;
    #endregion
    
    #region Public Methods

    #endregion


    #region Private Methods
    void Start()
    {
        StartCoroutine(Initialize());

    }//Startttttt


    IEnumerator Initialize()
    {
        _InformPanel.SetActive(false);
        UpdateHUD();
        yield return new WaitForSeconds(1f);
        GiftOfFirstTimePlaying();
    }

    void GiftOfFirstTimePlaying()
    {
        if (PlayerPrefs2.GetBool(DB.FirstGift))
        {
            _InformPanel.SetActive(false);
        }
        else
        {
            PlayerPrefs2.SetBool(DB.FirstGift, true);

            InformStruct giftStruct = _InformPanel.GetComponent<InformStruct>();

            giftStruct.GetTitle("Gift");
            giftStruct.GetExplain("Courtesy of first time playing!");

            giftStruct.GetHeartNumber(5);
            giftStruct.GetCoinNumber(1000);
            giftStruct.GetEnergyNumber(5);
            giftStruct.GetAppleNumber(300);

            _InformPanel.SetActive(true);
            UpdateHUD();
        }
    }


    void UpdateHUD()
    {
        //Getting Hearts Number.
        int hearts = HeartRepo.GetHeart();

        //Getting Coins Number.
        int coins = CoinRepo.GetCoin();

        //Getting EnergyBottles Number.
        int energyBottles = EnergyRepo.GetEnergyBottles();

        //Getting Apples' Number.
        int apples = AppleRepo.GetApples();





        //Updating Hearts IMG.
        if (hearts <= 0)
        {
            heartIMG.color = _Color.WB_DGray;
        }
        else
        {
            heartIMG.color = Color.white;
        }

        //Updating Coins IMG.
        if (coins <= 0)
        {
            coinIMG.color = _Color.WB_DGray;
        }
        else
        {
            coinIMG.color = Color.white;
        }

        //Updating EnergyBottles IMG.
        if (energyBottles <= 0)
        {
            energyIMG.color = _Color.WB_DGray;
        }
        else
        {
            energyIMG.color = Color.white;
        }

        //Updating Apple IMG.
        if (apples <= 0)
        {
            appleIMG.color = _Color.WB_DGray;
        }
        else
        {
            appleIMG.color = Color.white;
        }



        //Updating Hearts TXT.
        heartTXT.text = hearts.ToString();

        //Updating Coins TXT.
        coinTXT.text = coins.ToString();

        //Updating EnergyBottles TXT.
        energyTXT.text = energyBottles.ToString();

        //Updating Apple TXT.
        appleTXT.text = apples.ToString();
    }
    void ChangingListener(Button button , UnityAction unityAction)
    {
        button.onClick.RemoveAllListeners();
        button.onClick.AddListener(unityAction);
    }

    void Update()
    {


    }//Updateeeee
    #endregion
}//EndClasssss
