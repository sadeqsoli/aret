﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class UnlockPanel : MonoBehaviour
{
    //button for paying tax coin to learn lesson.
    [SerializeField] Button _PayButton;

    [SerializeField] TextMeshProUGUI _Explain_Pay, _CoinNumberTXT;


    public void SetPayBtn_Listener(UnityAction unityAction)
    {
        _PayButton.onClick.RemoveAllListeners();
        _PayButton.onClick.AddListener(unityAction);
    }
    public void SetInteractable_Listener(bool isInteractable)
    {
        _PayButton.interactable = isInteractable;
    }

    public void GetExplainPayTXT(string payInstructions)
    {
        _Explain_Pay.text = payInstructions;
    }
    public void GetCoinTXT(int taxNumb)
    {
        _CoinNumberTXT.text = taxNumb.ToString();
    }
}
