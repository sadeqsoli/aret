﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections;
using UnityEngine.Networking;
using System.IO;

public class AR_LessonsList : MonoBehaviour
{
    #region Properties

    #endregion

    #region Fields

    //Prefab Button.
    [SerializeField] GameObject lessonButtonPrefab;

    //Content of Lessons.
    [SerializeField] GameObject _Lessons_Content;

    //Content of Lessons.
    [SerializeField] GameObject _UnlockPanel, goToShopPanel , _DownPanel, _SubscribePanel;

    //Content of Lessons.
    [SerializeField] TextMeshProUGUI _ProgressTXT;

    //Content of Lessons.
    [SerializeField] Image _ProgressIMG;

    //Status Sprite of Lessons.
    [SerializeField] Sprite[] markSprite;

    //showing download progress by number and visual.
    float progress = 0f;

    //showing download progress by number and visual.
    float delay = 0.01f;

    const int taxMulti = 500;
    #endregion

    #region Public Methods
    #endregion


    #region Private Methods
    void Start()
    {
        SetAll_AR_Lessons();
    }//Startttttt


    void SetAll_AR_Lessons()
    {
        Removing_AR_LessonButtons();
        int currentAR_Lesson = Lesson_Repo.GetPassed_AR_Lesson() + 1;
        string lessonName = "";
        for (int i = 1; i < DB.LessonsLength; i++)
        {
            GameObject gameObj = Instantiate(lessonButtonPrefab);
            gameObj.transform.SetParent(_Lessons_Content.transform, false);

            _LessonButton AR_LessonButton = gameObj.GetComponent<_LessonButton>();
            lessonName = "Lesson " + (i).ToString();
            AR_LessonButton.GetLessonNameTXT(lessonName);
            int AR_L_Number = i;
            int coinNumber = AR_L_Number * taxMulti;
            int PaidLesson = PaidRepo.GetPaid_AR_Lessons(DB.Paid_AR_Lessons(AR_L_Number));
            if (Down_Repo.IsRepoHas(DB.IsDownRepo(AR_L_Number)))
            {
                AR_LessonButton.GetDownPanelStatus(false);
            }
            else
            {
                AR_LessonButton.GetDownPanelStatus(true);
            }
            AR_LessonButton.SetDownloadBtn_Listener(delegate { DownloadImages(AR_L_Number); });

            if (currentAR_Lesson < i)
            {
                //These are the lessons that are locked.
                AR_LessonButton.TaxNumber = coinNumber;
                AR_LessonButton.GetCoinTXT(coinNumber);
                AR_LessonButton.GetLessonColor(_Color.WB_SLGray);
                AR_LessonButton.GetStatus_IMG(markSprite[0]);
                AR_LessonButton.SetLessonBtn_Listener(
                        delegate { Locked_AR_Lesson(AR_L_Number, coinNumber); });

            }
            else if (currentAR_Lesson == i)
            {
                //this is the Current Lesson that user should Learn.   
                if (PaidLesson == 0)
                {
                    AR_LessonButton.GetCoinTXT(coinNumber);

                    AR_LessonButton.SetLessonBtn_Listener(
                            delegate { OpenCurrentUnpaid_AR_Lesson(AR_L_Number, coinNumber); });
                }
                else if (PaidLesson == 1)
                {
                    coinNumber = 0;
                    AR_LessonButton.GetCoinTXT(coinNumber);

                    AR_LessonButton.SetLessonBtn_Listener(
                            delegate { OpenCurrentPaid_AR_Lesson(AR_L_Number); });
                }
                AR_LessonButton.TaxNumber = coinNumber;
                AR_LessonButton.GetCoinTXT(coinNumber);
                AR_LessonButton.GetLessonColor(_Color.Y_Gold);
                AR_LessonButton.GetStatus_IMG(markSprite[1]);

            }
            else if (currentAR_Lesson > i)
            {
                //This Is the Lessons That User passed and Learned.
                AR_LessonButton.TaxNumber = 0;
                AR_LessonButton.GetCoinTXT(0);
                AR_LessonButton.GetLessonColor(_Color.G_LGreen);
                AR_LessonButton.GetStatus_IMG(markSprite[2]);
                AR_LessonButton.SetLessonBtn_Listener(
                            delegate { OpenPassed_AR_Lesson(AR_L_Number); });

            }
        }
    }





    void Locked_AR_Lesson(int AR_lessonNumb, int coinTax)
    {
        string explain = "Pay " + coinTax + " to unlock lesson " + AR_lessonNumb;
        UnlockPanel unlockPanel = _UnlockPanel.GetComponent<UnlockPanel>();
        unlockPanel.GetExplainPayTXT(explain);
        unlockPanel.GetCoinTXT(coinTax);
        unlockPanel.SetInteractable_Listener(true);

        unlockPanel.SetPayBtn_Listener(delegate { Current_AR_Lesson(AR_lessonNumb, coinTax); });
        _UnlockPanel.SetActive(true);
    }

    void OpenCurrentPaid_AR_Lesson(int AR_lessonNumb)
    {
        Lesson_Repo.SaveCurrent_AR_Lesson(AR_lessonNumb);
        Scener.GoToAR_Lesson();
    }
    void OpenCurrentUnpaid_AR_Lesson(int AR_lessonNumb, int coinTax)
    {
        string explain = "Pay " + coinTax + " to unlock lesson " + AR_lessonNumb;
        UnlockPanel unlockPanel = _UnlockPanel.GetComponent<UnlockPanel>();
        unlockPanel.GetExplainPayTXT(explain);
        unlockPanel.GetCoinTXT(coinTax);
        unlockPanel.SetInteractable_Listener(true);

        unlockPanel.SetPayBtn_Listener(delegate { Current_AR_Lesson(AR_lessonNumb, coinTax); });
        _UnlockPanel.SetActive(true);
    }



    void OpenPassed_AR_Lesson(int AR_lessonNumb)
    {
        Lesson_Repo.SaveCurrent_AR_Lesson(AR_lessonNumb);
        Scener.GoToAR_Lesson();
    }


    void Current_AR_Lesson(int AR_lessonNumb, int coinTax)
    {
        if (CoinRepo.HasCoin(coinTax))
        {
            _UnlockPanel.SetActive(false);
            CoinRepo.PopCoins(coinTax);
            PaidRepo.SetPaid_AR_Lesson(1, DB.Paid_AR_Lessons(AR_lessonNumb));
            Lesson_Repo.OpenNew_AR_Lesson(AR_lessonNumb);
            Lesson_Repo.SaveCurrent_AR_Lesson(AR_lessonNumb);
            //StartCoroutine(GoToARScene());
            Scener.GoToAR_Lesson();

        }
        else
        {
            goToShopPanel.SetActive(true);
        }
    }

    IEnumerator GoToARScene()
    {
        yield return new WaitForSeconds(1f);
        Scener.GoToAR_Lesson();
    }

    void DownloadImages(int LNumber)
    {
        _DownPanel.SetActive(true);
        StartCoroutine(DownloadingAssets(LNumber));
    }
    void Subscribe()
    {
        _SubscribePanel.SetActive(true);
    }



    //Method for download progress
    IEnumerator CheckProgress(float newProgress)
    {
        float count = progress + newProgress;
        if (count > 100f)
        {
            count = 100f;
            progress = 98.99f;
        }
        for (float i = progress; i < count; i += 0.37f)
        {
            progress = i + 1.2f;
            if (progress > 100f)
            {
                progress = 100.0f;
                _ProgressIMG.fillAmount = 1f;
                _ProgressTXT.text = "100 %";
                yield break;
            }
            _ProgressIMG.fillAmount = progress / 100f;
            _ProgressTXT.text = progress.ToString("###") + " %";
            yield return new WaitForSeconds(delay);
        }

    }

    IEnumerator DownloadingAssets(int LNumber)//downloading all of specific Plot's assets.
    {
        float pr = 100 / (16 - 2);
        for (int i = 1; i < 16; i++)
        {
            StartCoroutine(CheckProgress(pr));
            while (Application.internetReachability == NetworkReachability.NotReachable)
            {
                yield return null;
            }
            yield return StartCoroutine(DownloadImage(DB.IMGServerURL(LNumber, i), DB.IMGLocalURL(LNumber, i)));

        }
        SuccessfulDownload(LNumber);
    }
    //Downloading Images from specific link.
    IEnumerator DownloadImage(string serverUrl, string localUrl)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(serverUrl);
        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
            Debug.Log(serverUrl);
        }
        else
        {
            while (Application.internetReachability == NetworkReachability.NotReachable)
            {
                yield return null;
            }
            if (!Directory.Exists(localUrl))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(localUrl));
            }
            File.WriteAllBytes(localUrl, request.downloadHandler.data);
        }
    }

    void SuccessfulDownload(int LNumber)//Showing this panel after download completed.
    {
        Down_Repo.SetDownloaded(DB.IsDownRepo(LNumber));
        _DownPanel.SetActive(false);
        SetAll_AR_Lessons();
        progress = 0;
        _ProgressIMG.fillAmount = progress;
        Debuger.C_Log("Dowmload Ended", Colors.olive);
    }


    void Removing_AR_LessonButtons()
    {
        int lessonCount = _Lessons_Content.transform.childCount;
        if (lessonCount > 0)
            for (int i = 0; i < lessonCount; i++)
            {
                GameObject gameObject = _Lessons_Content.transform.GetChild(i).gameObject;
                Destroy(gameObject);
            }
    }


    void Update()
    {


    }//Updateeeee
    #endregion
}//EndClasssss
