﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;

public class AR_Game_Manager : Static_T<AR_Game_Manager>
{
    #region Properties

    #endregion

    #region Fields
    [SerializeField] GameObject _InformPanel, _WordStruct, _PronounPanel;


    [SerializeField] string[] learned_AR_Words = new string[0];

    const int CoinReward = 150;
    const int AppleRewardMulti = 50;
    #endregion

    #region Public Methods
    public void RewardingPronounTest()
    {
        int lessonNumber = Lesson_Repo.GetPassed_AR_Lesson();
        string word = TempRepo.GetTempWord();
        WordRepo.AddLearnedWords(word, DB.AR_Word_Repo(lessonNumber));
        RewardPronounTest();
        IsCurrent_AR_LessonDone(lessonNumber);
        _PronounPanel.SetActive(false);
        _WordStruct.SetActive(false);
        //AR_Lesson.Instance.UpdateVariables(lessonNumber);
    }

    #endregion


    #region Private Methods
    void Start()
    {
        
    }//Startttttt





    void RewardPronounTest()
    {
        InformStruct rewardStruct = _InformPanel.GetComponent<InformStruct>();

        rewardStruct.GetTitle("Reward");
        rewardStruct.GetExplain("Well Done " + UserRepo.GetUser() + "!");

        rewardStruct.GetCoinNumber(CoinReward);
        rewardStruct.GetEnergyNumber(1);
        rewardStruct.GetAppleNumber(AppleRewardMulti);

        _InformPanel.SetActive(true);
    }
    void RewardLessonPassed(int AR_LessonNumb)
    {
        InformStruct rewardStruct = _InformPanel.GetComponent<InformStruct>();

        rewardStruct.GetTitle("Reward");
        rewardStruct.GetExplain("Lesson " + AR_LessonNumb + " is over. Well Done " + UserRepo.GetUser() + "!");

        rewardStruct.GetCoinNumber(CoinReward * AR_LessonNumb);
        rewardStruct.GetHeartNumber(5);

        _InformPanel.SetActive(true);
        Lesson_Repo.OpenNew_AR_Lesson(AR_LessonNumb);
    }
 




    void IsCurrent_AR_LessonDone(int currentLesson)//put all of selected words and dialogues from repo to an array
    {

        int WordsCount = Reader.Instance.WordsLength;

        if (!string.IsNullOrEmpty(WordRepo.GetRepo(DB.AR_Word_Repo(currentLesson))))
        {
            learned_AR_Words = WordRepo.GetRepo(DB.AR_Word_Repo(currentLesson)).Split('/');
        }
        else
        {
            learned_AR_Words = new string[0];
        }
        if (learned_AR_Words.Length - 1 >= WordsCount)
        {
            RewardLessonPassed(currentLesson);
        }
        Debug.Log(learned_AR_Words.Length);
        Debug.Log(WordsCount);
    }



    void Update()
    {


    }//Updateeeee
    #endregion
}//EndClasssss
