﻿using UnityEngine;
using UnityEngine.Events;
using TMPro;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;
using System;

public class AR_WordList : Static_T<AR_WordList>
{
    #region Properties

    #endregion

    #region Fields
    [SerializeField] Button nextWord, prevWord;

    [SerializeField] Button uk_Pronoun, us_Pronoun;

    [SerializeField] GameObject IsDonePanel;
    //title TMPro.
    [SerializeField] TextMeshProUGUI titleTXT;
    //title TMPro.
    [SerializeField] TextMeshPro ThaWordTXT;

    [SerializeField] MeshRenderer meshShowBox;

    [SerializeField] List<Material> materials = new List<Material>();

    List<string> ar_Words = new List<string>();

    Vector3 startPos, inPos, endPos;

    string[] learned_AR_Words;
    int w = -1;
    int _current_AR_Lesson;
    #endregion

    #region Public Methods

    #endregion


    #region Private Methods
    void Start()
    {
        UpdateVariables();
        ChangeListener(nextWord, NextWord);
        ChangeListener(prevWord, PrevWord);
        Done(false);
    }//Startttttt

    void UpdateVariables()
    {
        w = -1;
        int current_AR_Lesson = Lesson_Repo.GetCurrent_AR_Lesson();
        _current_AR_Lesson = current_AR_Lesson;
        Reader.Instance.ReadCurrentLesson(current_AR_Lesson);
        titleTXT.text = "Lesson " + current_AR_Lesson;
        GetAllLearned_WS(current_AR_Lesson);
        Set_AR_Words(current_AR_Lesson);
        NextWord();
    }

    void GetAllLearned_WS(int current_AR_Lesson)//put all of selected words and dialogues from repo to an array
    {
        if (!string.IsNullOrEmpty(WordRepo.GetRepo(DB.AR_Word_Repo(current_AR_Lesson))))
        {
            learned_AR_Words = WordRepo.GetRepo(DB.AR_Word_Repo(current_AR_Lesson)).Split('/');
        }
        else
        {
            learned_AR_Words = new string[0];
        }
    }

    void Set_AR_Words(int current_AR_Lesson)
    {
        int WordsCount = Reader.Instance.WordsLength;

        for (int i = 0; i < WordsCount; i++)
        {
            string word = Reader.Instance.Words[i, 1];
            ar_Words.Add(word);
        }
        StartCoroutine(SetTextures(current_AR_Lesson, WordsCount));
    }
    IEnumerator SetTextures(int current_AR_Lesson, int WordsCount)
    {
        for (int i = 0; i < WordsCount; i++)
        {
            StartCoroutine(LoadImage(DB.IMGLocalURL(current_AR_Lesson, i + 1), i));
        }
        yield return new WaitForSeconds(0.4f);
    }



    void NextWord()
    {
        nextWord.interactable = false;
        w++;
        if (w > ar_Words.Count - 1)
        {
            Done(true);
            return;
        }
        string word = ar_Words[w];
        if (learned_AR_Words.Length > 0)
        {
            if (CompareStrings(learned_AR_Words, word))
            {
                ThaWordTXT.color = _Color.G_LGreen;
            }
            else
            {
                ThaWordTXT.color = Color.white;
            }
        }

        ChangeListener(uk_Pronoun, delegate { Speak_UK_Word(word); });
        ChangeListener(us_Pronoun, delegate { Speak_US_Word(word); });

        ThaWordTXT.text = word;

        if (w < materials.Count - 1)
            meshShowBox.material = materials[w];

    }
    void PrevWord()
    {
        nextWord.interactable = true;
        w--;
        if (w < 0)
        {
            w = 0;
            return;
        }
        string word = ar_Words[w];
        if (learned_AR_Words.Length > 0)
        {
            if (CompareStrings(learned_AR_Words, word))
            {
                ThaWordTXT.color = _Color.G_LGreen;
            }
            else
            {
                ThaWordTXT.color = Color.white;
            }
        }
        ChangeListener(uk_Pronoun, delegate { Speak_UK_Word(word); });
        ChangeListener(us_Pronoun, delegate { Speak_US_Word(word); });

        ThaWordTXT.text = word;

        if (w >= 0)
            meshShowBox.material = materials[w];
    }
    void Done(bool isDone)
    {
        IsDonePanel.SetActive(isDone);
        if (isDone == true)
        {
            Lesson_Repo.OpenNew_AR_Lesson(_current_AR_Lesson);
        }
    }


    //Word's TTS.
    void Speak_UK_Word(string word)
    {
        SpeakUp.Speak(word, GetAudioSource(), "Microsoft Zira Desktop");
        WordRepo.AddLearnedWords(word, DB.AR_Word_Repo(_current_AR_Lesson));
        nextWord.interactable = true;
    }
    void Speak_US_Word(string word)
    {
        SpeakUp.Speak(word, GetAudioSource(), "Microsoft David Desktop");
        WordRepo.AddLearnedWords(word, DB.AR_Word_Repo(_current_AR_Lesson));
        nextWord.interactable = true;
    }

    IEnumerator LoadImage(string localUrl, int numb)//loading images as texture2D.
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture("file://" + localUrl);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
        }
        else
        {
            Texture2D texture = ((DownloadHandlerTexture)request.downloadHandler).texture;
            materials[numb].mainTexture = texture;
        }
    }
    void ChangeListener(Button button, UnityAction unityAction)
    {
        button.onClick.RemoveAllListeners();
        button.onClick.AddListener(unityAction);
    }
    bool CompareStrings(string[] learnedWords, string word)//Comparing Words and sentences.
    {
        for (int i = 0; i < learnedWords.Length; i++)
        {
            if (learnedWords[i].ToLower() == word.ToLower())
            {
                return true;
            }
        }
        return false;
    }
    AudioSource GetAudioSource()
    {
        return FindObjectOfType<AudioSource>();
    }




    void Update()
    {


    }//Updateeeee
    #endregion
}//EndClasssss
