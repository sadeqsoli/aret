﻿using UnityEngine;
using UnityEngine.Events;
using TMPro;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public class _WordList : Static_T<_WordList>
{
    #region Properties

    #endregion

    #region Fields
    //title TMPro.
    [SerializeField] TextMeshProUGUI titleTXT;

    //Prefab Button.
    [SerializeField] GameObject wordButtonPrefab;

    //Content of Word Buttons.
    [SerializeField] GameObject _Word_Content;

    //Content of Word Buttons.
    [SerializeField] GameObject _Word_Struct, _Word_Struct_Content;

    //Content of Word Buttons.
    [SerializeField] GameObject PronounTestPanel;

    Texture2D[] textures = new Texture2D[15];

    string[] learnedWords;

    #endregion

    #region Public Methods
    public void UpdateAllWordInCurrentLesson()
    {
        int currentLesson = Lesson_Repo.GetCurrentLesson();
        UpdateVariables(currentLesson);
    }
    #endregion


    #region Private Methods
    void Start()
    {
        UpdateAllWordInCurrentLesson();
    }//Startttttt

    public void UpdateVariables(int currentLesson)
    {
        Reader.Instance.ReadCurrentLesson(currentLesson);
        GetAllLearned_WS(currentLesson);
        GetIMGs(currentLesson);
        SetWords(currentLesson);
    }

    void GetAllLearned_WS(int currentLesson)//put all of selected words and dialogues from repo to an array
    {

        if (!string.IsNullOrEmpty(WordRepo.GetRepo(DB.Word_Repo(currentLesson))))
        {
            learnedWords = WordRepo.GetRepo(DB.Word_Repo(currentLesson)).Split('/');
        }
        else
        {
            learnedWords = new string[0];
        }
    }
    void GetIMGs(int currentLesson)
    {
        textures = new Texture2D[0]; textures = new Texture2D[15];

        for (int i = 0; i < 15; i++)
        {
            int wordNumb = i + 1;
            StartCoroutine(LoadImage(DB.IMGLocalURL(currentLesson, wordNumb), i));
        }

    }
    void SetWords(int currentLesson)
    {
        titleTXT.text = "Lesson " + currentLesson;

        int WordsCount = Reader.Instance.WordsLength;

        RemovingWordButtons();
        for (int i = 0; i < WordsCount; i++)
        {
            int currentWordNumb = i;
            GameObject gameObj = Instantiate(wordButtonPrefab);
            gameObj.transform.SetParent(_Word_Content.transform, false);
            _WordButton _WordScript = gameObj.GetComponent<_WordButton>();


            string word = Reader.Instance.Words[i, 1];
            _WordScript.GetWordNumbTXT(Reader.Instance.Words[i, 0]);
            _WordScript.GetWordTXT(word);

            //Adding Listener for Word Button.
            _WordScript.ChangeWordButtonListener(delegate { OpenCurrentWord(currentWordNumb); });

            //Adding Listener for UK Voice Button.
            _WordScript.Change_UK_VoiceButtonListener(delegate { Speak_UK_Word(word); });

            //Adding Listener for US Voice Button.
            _WordScript.Change_US_VoiceButtonListener(delegate { Speak_US_Word(word); });


            //Determine if Word is Selected or not.
            if (learnedWords.Length > 0)
            {
                if (CompareStrings(learnedWords, word))
                {
                    _WordScript.GetWordNumbColor(_Color.G_LGreen);
                    _WordScript.GetButtonColor(_Color.G_SLGreen);
                }
                else
                {
                    _WordScript.GetWordNumbColor(_Color.WB_LGray);
                    _WordScript.GetButtonColor(_Color.WB_SLGray);
                }
            }

        }
        StartCoroutine(SetChilds(_Word_Content, 0.02f));
        StartCoroutine(SetAllIMG());
    }
    IEnumerator SetAllIMG()
    {
        yield return new WaitForSeconds(1f);
        for (int i = 0; i < _Word_Content.transform.childCount; i++)
        {
            GameObject gameObj = _Word_Content.transform.GetChild(i).gameObject;
            _WordButton _WordScript = gameObj.GetComponent<_WordButton>();
            _WordScript.GetEx_Image(textures[i]);
        }
    }


    void OpenCurrentWord(int currentWord)
    {
        _Word_Struct.SetActive(true);
        _WordStruct _WordStruct = _Word_Struct.GetComponent<_WordStruct>();
        string tha_Word = Reader.Instance.Words[currentWord, 1];

        string word_Type = Reader.Instance.Words[currentWord, 2];

        string word_Syn = Reader.Instance.Words[currentWord, 3];

        string word_FA = Reader.Instance.Words[currentWord, 4];


        string word_EN_EX = Reader.Instance.Words[currentWord, 5];

        string word_FA_EX = Reader.Instance.Words[currentWord, 6];

        string word_AR_Address = Reader.Instance.Words[currentWord, 7];

        _WordStruct.GetWordNumberTXT((currentWord + 1).ToString());

        _WordStruct.GetWordTXT(tha_Word + " (" + word_Type + ")");

        _WordStruct.Get_FA_WordTXT(word_FA);

        _WordStruct.GetEN_SynonymTXT(word_Syn);

        _WordStruct.GetEN_ExTXT(word_EN_EX);

        _WordStruct.GetFA_ExTXT(word_FA_EX);

        _WordStruct.GetExplain_Image(textures[currentWord]);

        _WordStruct.Change_Tha_Word_US_Listener(delegate { Speak_US_Word(tha_Word); SetInteractablity_ON(_WordStruct); });
        _WordStruct.Change_Tha_Word_UK_Listener(delegate { Speak_UK_Word(tha_Word); SetInteractablity_ON(_WordStruct); });

        _WordStruct.Change_Example_US_Listener(delegate { Speak_US_Word(word_EN_EX); });
        _WordStruct.Change_Example_UK_Listener(delegate { Speak_UK_Word(word_EN_EX); });

        _WordStruct.Change_Pronoun_Listener(delegate { GetCurrentWord(word_EN_EX, tha_Word); }, word_EN_EX);
        _WordStruct.Change_Interactablity(false);


        StartCoroutine(SetChilds(_Word_Struct_Content, 0.0000001f));
    }

    void SetInteractablity_ON(_WordStruct _WordStruct)
    {
        _WordStruct.Change_Interactablity(true);
    }
    void GetCurrentWord(string example, string tha_Word)
    {
        TempRepo.PushTempWord(tha_Word);
        GetSpeechPercent.CurrentTalk(example);
        PronounTestPanel.SetActive(true);
    }

    //Word's TTS.
    void Speak_UK_Word(string word)
    {
        SpeakUp.Speak(word, GetAudioSource(), "Microsoft Zira Desktop");
    }
    void Speak_US_Word(string word)
    {
        SpeakUp.Speak(word, GetAudioSource(), "Microsoft David Desktop");
    }




    IEnumerator LoadImage(string localUrl, int numb)//loading images as texture2D.
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture("file://" + localUrl);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
        }
        else
        {
            Texture2D texture = ((DownloadHandlerTexture)request.downloadHandler).texture;
            textures[numb] = texture;
        }
    }

    bool CompareStrings(string[] learnedWords, string word)//Comparing Words and sentences.
    {
        for (int i = 0; i < learnedWords.Length; i++)
        {
            if (learnedWords[i].ToLower() == word.ToLower())
            {
                return true;
            }
        }
        return false;
    }
    IEnumerator LoadImage(string localUrl)//loading images as texture2D.
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture("file://" + localUrl);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
        }
        else
        {
            Texture2D texture = ((DownloadHandlerTexture)request.downloadHandler).texture;
            //frames.Add(texture);
        }
    }
    AudioSource GetAudioSource()
    {
        return FindObjectOfType<AudioSource>();
    }
    IEnumerator SetChilds(GameObject gameObject, float delay)
    {
        if (gameObject.transform.childCount > 0)
        {
            for (int i = 0; i < gameObject.transform.childCount; i++)
            {
                gameObject.transform.GetChild(i).gameObject.SetActive(false);
                yield return new WaitForSeconds(delay);
            }
            for (int i = 0; i < gameObject.transform.childCount; i++)
            {
                gameObject.transform.GetChild(i).gameObject.SetActive(true);
                yield return new WaitForSeconds(delay);
            }
        }
    }
    void RemovingWordButtons()
    {
        int wordCount = _Word_Content.transform.childCount;
        if (wordCount > 0)
            for (int i = 0; i < wordCount; i++)
            {
                GameObject gameObject = _Word_Content.transform.GetChild(i).gameObject;
                Destroy(gameObject);
            }
    }


    void Update()
    {


    }//Updateeeee
    #endregion
}//EndClasssss
