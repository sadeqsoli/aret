﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;

public class Game_Manager : Static_T<Game_Manager>
{
    #region Properties

    #endregion

    #region Fields
    [SerializeField] GameObject _InformPanel, _WordStruct, _PronounPanel;


    [SerializeField] string[] learnedWords = new string[0];

    const int CoinReward = 40;
    const int AppleRewardMulti = 25;
    #endregion

    #region Public Methods
    public void RewardingPronounTest()
    {
        int lessonNumber = Lesson_Repo.GetPassedLesson();
        string word = TempRepo.GetTempWord();
        WordRepo.AddLearnedWords(word, DB.Word_Repo(lessonNumber));
        RewardPronounTest();
        IsCurrentLessonDone(lessonNumber);
        _PronounPanel.SetActive(false);
        _WordStruct.SetActive(false);
        _WordList.Instance.UpdateVariables(lessonNumber);

    }

    #endregion


    #region Private Methods
    void Start()
    {
    }//Startttttt



    void RewardPronounTest()
    {
        InformStruct rewardStruct = _InformPanel.GetComponent<InformStruct>();

        rewardStruct.GetTitle("Reward");
        rewardStruct.GetExplain("Well Done " + UserRepo.GetUser() + "!");

        rewardStruct.GetCoinNumber(CoinReward);
        rewardStruct.GetEnergyNumber(1);
        rewardStruct.GetAppleNumber(AppleRewardMulti);

        _InformPanel.SetActive(true);
    }
    void RewardLessonPassed(int lessonNumb)
    {
        InformStruct rewardStruct = _InformPanel.GetComponent<InformStruct>();

        rewardStruct.GetTitle("Reward");
        rewardStruct.GetExplain("Lesson " + lessonNumb + " is over. Well Done " + UserRepo.GetUser() + "!");

        rewardStruct.GetCoinNumber(CoinReward * lessonNumb);
        rewardStruct.GetHeartNumber(5);

        _InformPanel.SetActive(true);

        Lesson_Repo.OpenNewLesson(lessonNumb);
    }
 




    void IsCurrentLessonDone(int currentLesson)//put all of selected words and dialogues from repo to an array
    {

        int WordsCount = Reader.Instance.WordsLength;

        if (!string.IsNullOrEmpty(WordRepo.GetRepo(DB.Word_Repo(currentLesson))))
        {
            learnedWords = WordRepo.GetRepo(DB.Word_Repo(currentLesson)).Split('/');
        }
        else
        {
            learnedWords = new string[0];
        }
        if (learnedWords.Length - 1 >= WordsCount)
        {
            RewardLessonPassed(currentLesson);
        }
        Debug.Log(learnedWords.Length);
        Debug.Log(WordsCount);
    }



    void Update()
    {


    }//Updateeeee
    #endregion
}//EndClasssss
