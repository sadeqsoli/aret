﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections;
using UnityEngine.Networking;
using System.IO;
using UnityEngine.Events;

public class LessonsList : MonoBehaviour
{
    #region Properties

    #endregion

    #region Fields
    //Prefab Button.
    [SerializeField] GameObject _lessonStruct;

    //Prefab Button.
    [SerializeField] GameObject lessonButtonPrefab;

    //Content of Lessons.
    [SerializeField] GameObject _Lessons_Content;

    //Content of Lessons.
    [SerializeField] GameObject _UnlockPanel, goToShopPanel, _DownPanel, _SubscribePanel;

    //Content of Lessons.
    [SerializeField] TextMeshProUGUI _ProgressTXT;

    //Content of Lessons.
    [SerializeField] Image _ProgressIMG;

    //Status Sprite of Lessons.
    [SerializeField] Sprite[] markSprite;

    //showing download progress by number and visual.
    float progress = 0f;

    //showing download progress by number and visual.
    float delay = 0.01f;

    const int taxMulti = 100;
    #endregion

    #region Public Methods
    #endregion


    #region Private Methods
    void Start()
    {
        SetAllLessons();
    }//Startttttt


    public void SetAllLessons()
    {
        RemovingLessonButtons();
        int currentLesson = Lesson_Repo.GetPassedLesson() + 1;
        string lessonName = "";
        for (int i = 1; i < DB.LessonsLength; i++)
        {
            GameObject gameObj = Instantiate(lessonButtonPrefab);
            gameObj.transform.SetParent(_Lessons_Content.transform, false);

            _LessonButton lessonButton = gameObj.GetComponent<_LessonButton>();
            lessonName = "Lesson " + (i).ToString();
            lessonButton.GetLessonNameTXT(lessonName);
            int L_Number = i;
            int coinNumber = L_Number * taxMulti;
            int PaidLesson = PaidRepo.GetPaidLessons(DB.PaidLessons(L_Number));
            if (Down_Repo.IsRepoHas(DB.IsDownRepo(L_Number)))
            {
                lessonButton.GetDownPanelStatus(false);
            }
            else
            {
                lessonButton.GetDownPanelStatus(true);
            }
            if(L_Number <= 5)
            {
                lessonButton.SetDownloadBtn_Listener(delegate { DownloadImages(L_Number); });

            }
            else
            {
                lessonButton.SetDownloadBtn_Listener(Subscribe);
            }

            if (currentLesson < i)
            {
                //These are the lessons that are locked.
                lessonButton.TaxNumber = coinNumber;
                lessonButton.GetCoinTXT(coinNumber);
                lessonButton.GetLessonColor(_Color.WB_SLGray);
                lessonButton.GetStatus_IMG(markSprite[0]);

                lessonButton.SetLessonBtn_Listener(
                        delegate { LockedLesson(L_Number, coinNumber); });

            }
            else if (currentLesson == i)
            {
                //this is the Current Lesson that user should Learn.   
                if (PaidLesson == 0)
                {
                    lessonButton.GetCoinTXT(coinNumber);

                    lessonButton.SetLessonBtn_Listener(
                            delegate { OpenCurrentUnpaid_Lesson(L_Number, coinNumber); });
                }
                else if (PaidLesson == 1)
                {
                    coinNumber = 0;
                    lessonButton.GetCoinTXT(coinNumber);
                    lessonButton.SetLessonBtn_Listener(
                            delegate { OpenCurrentPaid_Lesson(L_Number); });
                }
                lessonButton.TaxNumber = coinNumber;
                lessonButton.GetCoinTXT(coinNumber);
                lessonButton.GetLessonColor(_Color.Y_Gold);
                lessonButton.GetStatus_IMG(markSprite[1]);

            }
            else if (currentLesson > i)
            {
                //This Is the Lessons That User passed and Learned.
                lessonButton.TaxNumber = 0;
                if (PaidLesson == 0)
                {
                    lessonButton.GetCoinTXT(coinNumber);
                }
                else if (PaidLesson == 1)
                {
                    lessonButton.GetCoinTXT(0);
                }
                lessonButton.GetLessonColor(_Color.G_LGreen);
                lessonButton.GetStatus_IMG(markSprite[2]);
                lessonButton.SetLessonBtn_Listener(delegate { OpenPassedLesson(L_Number); });

            }
        }
    }





    void LockedLesson(int _lessonNumb, int coinTax)
    {
        string explain = "Pay " + coinTax + " to unlock lesson " + _lessonNumb;
        UnlockPanel unlockPanel = _UnlockPanel.GetComponent<UnlockPanel>();
        unlockPanel.GetExplainPayTXT(explain);
        unlockPanel.GetCoinTXT(coinTax);
        unlockPanel.SetInteractable_Listener(true);

        unlockPanel.SetPayBtn_Listener(delegate { CurrentLesson(_lessonNumb, coinTax); });
        _UnlockPanel.SetActive(true);
    }
    void OpenCurrentPaid_Lesson(int _lessonNumb)
    {
        _lessonStruct.SetActive(true);
        Lesson_Repo.SaveCurrentLesson(_lessonNumb);
        _WordList.Instance.UpdateVariables(_lessonNumb);
    }
    void OpenCurrentUnpaid_Lesson(int _lessonNumb, int coinTax)
    {
        string explain = "Pay " + coinTax + " to unlock lesson " + _lessonNumb;
        UnlockPanel unlockPanel = _UnlockPanel.GetComponent<UnlockPanel>();
        unlockPanel.GetExplainPayTXT(explain);
        unlockPanel.GetCoinTXT(coinTax);
        unlockPanel.SetInteractable_Listener(true);

        unlockPanel.SetPayBtn_Listener(delegate { CurrentLesson(_lessonNumb, coinTax); });
        _UnlockPanel.SetActive(true);
    }



    void OpenPassedLesson(int _lessonNumb)
    {
        _lessonStruct.SetActive(true);
        Lesson_Repo.SaveCurrentLesson(_lessonNumb);
        _WordList.Instance.UpdateVariables(_lessonNumb);
    }


    void CurrentLesson(int _lessonNumb, int coinTax)
    {
        if (CoinRepo.HasCoin(coinTax))
        {
            _lessonStruct.SetActive(true);
            _UnlockPanel.SetActive(false);
            CoinRepo.PopCoins(coinTax);
            PaidRepo.SetPaidLesson(1, DB.PaidLessons(_lessonNumb));
            _WordList.Instance.UpdateVariables(_lessonNumb);
            Lesson_Repo.OpenNewLesson(_lessonNumb);
            Lesson_Repo.SaveCurrentLesson(_lessonNumb);
        }
        else
        {
            goToShopPanel.SetActive(true);
        }
    }

    void DownloadImages(int LNumber)
    {
        _DownPanel.SetActive(true);
        StartCoroutine(DownloadingAssets(LNumber));
    }
    void Subscribe()
    {
        _SubscribePanel.SetActive(true);
    }



    IEnumerator CheckProgress(float newProgress)
    {
        float count = progress + newProgress;
        if (count > 100f)
        {
            count = 100f;
            progress = 98.99f;
        }
        for (float i = progress; i < count; i += 0.37f)
        {
            progress = i + 1.2f;
            if (progress > 100f)
            {
                progress = 100.0f;
                _ProgressIMG.fillAmount = 1f;
                _ProgressTXT.text = "100 %";
                yield break;
            }
            _ProgressIMG.fillAmount = progress / 100f;
            _ProgressTXT.text = progress.ToString("###") + " %";
            yield return new WaitForSeconds(delay);
        }
    }

    IEnumerator DownloadingAssets(int LNumber)//downloading all of specific Plot's assets.
    {
        float pr = 100 / (16 - 2);
        for (int i = 1; i < 16; i++)
        {
            StartCoroutine(CheckProgress(pr));
            while (Application.internetReachability == NetworkReachability.NotReachable)
            {
                yield return null;
            }
            yield return StartCoroutine(DownloadImage(DB.IMGServerURL(LNumber, i), DB.IMGLocalURL(LNumber, i)));

        }
        SuccessfulDownload(LNumber);
    }


    void SuccessfulDownload(int LNumber)//Showing this panel after download completed.
    {
        Down_Repo.SetDownloaded(DB.IsDownRepo(LNumber));
        _DownPanel.SetActive(false);
        SetAllLessons();
        progress = 0;
        _ProgressIMG.fillAmount = progress;
        Debuger.C_Log("Dowmload Ended", Colors.olive);
    }



    //Downloading Images from specific link.
    IEnumerator DownloadImage(string serverUrl, string localUrl)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(serverUrl);
        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
            Debug.Log(serverUrl);
        }
        else
        {
            while (Application.internetReachability == NetworkReachability.NotReachable)
            {
                yield return null;
            }
            if (!Directory.Exists(localUrl))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(localUrl));
            }
            File.WriteAllBytes(localUrl, request.downloadHandler.data);
        }
    }









    void RemovingLessonButtons()
    {
        int lessonCount = _Lessons_Content.transform.childCount;
        if (lessonCount > 0)
            for (int i = 0; i < lessonCount; i++)
            {
                GameObject gameObject = _Lessons_Content.transform.GetChild(i).gameObject;
                Destroy(gameObject);
            }
    }


    void Update()
    {


    }//Updateeeee
    #endregion
}//EndClasssss
