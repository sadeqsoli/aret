﻿using TMPro;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CorrectPanel : MonoBehaviour
{
    #region Properties

    #endregion

    #region Fields

    [Space]
    /// necessary game objects that need to off/on. 
    [SerializeField] GameObject isCorrectPanel;

    /// Image of true or false.
    [SerializeField] Image coinIMG;

    /// Correct Dialogue.
    [SerializeField] TextMeshProUGUI userAnswerTXT;

    /// necessary Texts for UI of Correct Check after quiz
    [SerializeField] TextMeshProUGUI isCorrectTXT, correctnessTXT;

    /// necessary Texts for UI of Correct Check after quiz
    [SerializeField] Button OKButton;

    /// necessary Texts for UI of Correct Check after quiz
    [SerializeField] TextMeshProUGUI advice_ComplimentTXT, userForcedComment;
    RecordManager recordManager;
    #endregion

    #region Public Methods

    //Word's STT.
    public void SpeechResult()//its gonna check for LS or PLS and Then IF its Correct Or Not.
    {
        recordManager.StopRecording();
        if (GetSpeechPercent.IsCorrect == true)
        {
            IsCorrectPanel_Speech(true);
        }
        else
        {
            IsCorrectPanel_Speech(false);
        }
    }

    #endregion


    #region Private Methods
    void Start()
    {
        recordManager = FindObjectOfType<RecordManager>();
    }//Startttttt


    void RandomComment(bool isCorrect)
    {
        int rnd = Random.Range(0, 4);
        if (isCorrect)
        {
            switch (rnd)
            {
                case 0:
                    advice_ComplimentTXT.text = "Awesome!";
                    break;
                case 1:
                    advice_ComplimentTXT.text = "Very Nice!";
                    break;
                case 2:
                    advice_ComplimentTXT.text = "Epic!";
                    break;
                case 3:
                    advice_ComplimentTXT.text = "OMG!!!";
                    break;
                case 4:
                    advice_ComplimentTXT.text = "Perfect!";
                    break;
            }
        }
        else
        {
            switch (rnd)
            {
                case 0:
                    advice_ComplimentTXT.text = "Try Again!";
                    break;
                case 1:
                    advice_ComplimentTXT.text = "Do It Again!";
                    break;
                case 2:
                    advice_ComplimentTXT.text = "Again!";
                    break;
                case 3:
                    advice_ComplimentTXT.text = "Maybe Next Time!";
                    break;
                case 4:
                    advice_ComplimentTXT.text = "Nice Try!";
                    break;
            }
        }
    }
    void ChangingListener(UnityAction unityAction)
    {
        OKButton.onClick.RemoveAllListeners();
        OKButton.onClick.AddListener(unityAction);
    }
    void CloseCorrectPanel()
    {
        isCorrectPanel.SetActive(false);
        correctnessTXT.color = Color.black;
        isCorrectPanel.GetComponentInChildren<Image>().color = Color.white;
        coinIMG.color = Color.white;
        correctnessTXT.text = "";
        isCorrectTXT.text = "";
        userAnswerTXT.text = "";
    }//Closing correct panel.

    
    void IsCorrectPanel_Speech(bool isCorrect)//for showing response that answer is correct or not.
    {
        if (isCorrect)
        {
            correctnessTXT.color = Color.black;
            correctnessTXT.text = "%" + GetSpeechPercent.CorrectnessPercent().ToString("##.#");

            isCorrectTXT.color = _Color.G_SDGreen;
            isCorrectTXT.text = "Correct!";

            coinIMG.color = Color.white;
            userAnswerTXT.text = RecordManager.Result;

            isCorrectPanel.GetComponentInChildren<Image>().color = _Color.G_TrueGreen;

            userForcedComment.text = "I know!";
            RandomComment(isCorrect);

            ChangingListener(CloseCorrectPanel);

            Game_Manager.Instance.RewardingPronounTest();

            isCorrectPanel.SetActive(true);
        }
        else
        {
            correctnessTXT.color = _Color.WB_SLGray;
            if (GetSpeechPercent.CorrectnessPercent() <= 0)
            {
                correctnessTXT.text = "% 00.0";
            }
            else
            {
                correctnessTXT.text = "%" + GetSpeechPercent.CorrectnessPercent().ToString("##.#");
            }
            
            isCorrectTXT.text = "Wrong!";
            isCorrectTXT.color = _Color.R_SDRed;

            coinIMG.color = _Color.WB_SDGray;
            if(RecordManager.Result == null)
            {
                userAnswerTXT.text = "Say the word above!";
            }
            else
            {
                userAnswerTXT.text = RecordManager.Result;
            }
            userForcedComment.text = "I will!";
            RandomComment(isCorrect);

            isCorrectPanel.GetComponentInChildren<Image>().color = _Color.R_FalseRed;
            isCorrectPanel.SetActive(true);

            ChangingListener(CloseCorrectPanel);
        }
    }

    void Update()
    {


    }//Updateeeee
    #endregion
}
