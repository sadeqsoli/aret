﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[CreateAssetMenu(menuName ="CreateLessonStruct")]
public class AR_LessonStruct : ScriptableObject
{
    [SerializeField] Material[] materials;

    public Material GetCurrentMaterial(int i)
    {
        return materials[i];
    }
}
