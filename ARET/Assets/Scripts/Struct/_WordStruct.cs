﻿using UnityEngine;
using RTLTMPro;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;

public class _WordStruct : MonoBehaviour
{
    #region Properties

    #endregion

    #region Fields
    //Image Explainationary to current Word.
    [SerializeField] RawImage Ex_Image;

    //Image Explainationary to current Word.
    [SerializeField] TextMeshProUGUI WordNumberTXT,_ThaWordTXT, EN_SynonymTXT, EN_ExampleTXT, ThaWord_SpeechTXT;

    //word Number Text.
    [SerializeField] RTLTextMeshPro FA_ThaWordTXT, FA_ExampleTXT;

    //Opening WordStructure.
    [SerializeField] Button _Pronoun_Test;

    //Pronounciation Buttons.
    [SerializeField] Button W_UK_Button, W_US_Button, EX_UK_Button, EX_US_Button;
    #endregion

    #region Public Methods
    public void GetExplain_Image(Texture2D newTexture)
    {
        Ex_Image.texture = newTexture;
    }
    public void GetWordNumberTXT(string text)
    {
        WordNumberTXT.text = "Word " + text;
    }
    public void GetWordTXT(string text)
    {
        _ThaWordTXT.text = text;
    }
    public void Get_FA_WordTXT(string text)
    {
        FA_ThaWordTXT.text = text;
    }
    public void GetEN_SynonymTXT(string text)
    {
        EN_SynonymTXT.text = text;
    }
    public void GetEN_ExTXT(string text)
    {
        EN_ExampleTXT.text = text;
    }
    public void GetFA_ExTXT(string text)
    {
        FA_ExampleTXT.text = text;
    }

    public void Change_Tha_Word_US_Listener(UnityAction unityAction)
    {
        W_US_Button.onClick.RemoveAllListeners();
        W_US_Button.onClick.AddListener(unityAction);
    }
    public void Change_Tha_Word_UK_Listener(UnityAction unityAction)
    {
        W_UK_Button.onClick.RemoveAllListeners();
        W_UK_Button.onClick.AddListener(unityAction);
    }
    public void Change_Example_US_Listener(UnityAction unityAction)
    {
        EX_US_Button.onClick.RemoveAllListeners();
        EX_US_Button.onClick.AddListener(unityAction);
    }
    public void Change_Example_UK_Listener(UnityAction unityAction)
    {
        EX_UK_Button.onClick.RemoveAllListeners();
        EX_UK_Button.onClick.AddListener(unityAction);
    }
    public void Change_Pronoun_Listener(UnityAction unityAction, string example)
    {
        _Pronoun_Test.onClick.RemoveAllListeners();
        _Pronoun_Test.onClick.AddListener(unityAction);
        ThaWord_SpeechTXT.text = example;
    }
    public void Change_Interactablity(bool isInteractable)
    {
        _Pronoun_Test.interactable = isInteractable;
    }
    #endregion


    #region Private Methods
    void Start()
    {

    }//Startttttt





    void Update()
    {


    }//Updateeeee
    #endregion
}//EndClasssss
