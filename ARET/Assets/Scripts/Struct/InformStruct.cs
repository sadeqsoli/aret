﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class InformStruct : MonoBehaviour
{
    [SerializeField] GameObject _HeartObj,_CoinObj,_EnergyObj, _AppleObj;

    [SerializeField] TextMeshProUGUI _TitleTXT,_ExplainTXT,_HeartTXT, _CoinTXT, _EnergyTXT, _AppleTXT;

    [SerializeField] Button OkButton;

    [SerializeField] Animator Scaler;

    

    public void GetTitle(string title)
    {
        _TitleTXT.text = title;
    }
    public void GetExplain(string explain)
    {
        _ExplainTXT.text = explain;
    }
    public void GetHeartNumber(int hearts)
    {
        HeartRepo.PushHearts(hearts);
        _HeartObj.SetActive(true);
        _HeartTXT.text = hearts.ToString();
    }
    public void GetCoinNumber(int coins)
    {
        CoinRepo.PushCoins(coins);
        _CoinObj.SetActive(true);
        _CoinTXT.text = coins.ToString();
    }
    public void GetEnergyNumber(int energyBottles)
    {
        EnergyRepo.PushEnergyBottle(energyBottles);
        _EnergyObj.SetActive(true);
        _EnergyTXT.text = energyBottles.ToString();
    }
    public void GetAppleNumber(int apples)
    {
        AppleRepo.PushApple(apples);
        _AppleObj.SetActive(true);
        _AppleTXT.text = apples.ToString();
    }
    public void SetInformPanelOFF()
    {
        gameObject.SetActive(false);
    }


    void Start()
    {
        OkButton.onClick.AddListener(SetScaleDownAnim);
    }//Starttttt

    void SetScaleDownAnim()
    {
        Scaler.SetTrigger("scaleDown");
    }

}//EndClassssss
